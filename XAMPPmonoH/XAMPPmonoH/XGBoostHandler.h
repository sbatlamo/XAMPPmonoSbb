#ifdef USE_XGBOOST
#ifndef XAMPPMonoH_XGBoostHandler_H
#define XAMPPMonoH_XGBoostHandler_H

#include <XAMPPbase/AnalysisUtils.h>

typedef void *BoosterHandle;

namespace XAMPP {
    class XGBoostHandler {
    public:
        XGBoostHandler();
        ~XGBoostHandler();

        /**
         * @brief       Calculate the XGBoost BDT output with given BDT model,
         *              variables, and number of variables.
         */
        float get_XGBoost_Weight(const std::vector<float> &vars, int event_number);
        bool LoadXGBoostModel(const std::string &region);
        bool initialize();

    private:
        BoosterHandle m_xgboost;
        BoosterHandle m_xgboost_r;
    };
}  // namespace XAMPP

#endif  //> !XAMPPMonoH_XGBoostHandler_H
#endif  //> USE_XGBOOST

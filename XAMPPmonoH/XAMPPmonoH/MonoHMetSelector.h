#ifndef XAMPPmonoH_MonoHMetSelector_H
#define XAMPPmonoH_MonoHMetSelector_H

#include <XAMPPbase/SUSYMetSelector.h>
namespace CP {

    /**
     * @brief      Interface class for MET trigger scale factor tool
     */
    class IMetTriggerSF;
}  // namespace CP
namespace XAMPP {
    class MonoHMetSelector : public SUSYMetSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(MonoHMetSelector, XAMPP::IMetSelector)

        /**
         * @brief      Default constructor
         *
         * @param[in]  myname  The name of the class instance
         */
        MonoHMetSelector(std::string myname);

        /**
         * @brief      Default destructor
         */
        virtual ~MonoHMetSelector();

        /**
         * @brief      Re-implementation of the parent class initialize method,
         *             calls parent class initialize method. Initializes also
         *
         *             - MetTSTlepInvis
         *
         *             If m_store_significance_lepInvis is enabled also the
         *             object based met significance tool
         *             "noPUJets_noSoftTerm_lepInvis" will be initialized.
         *
         *             Note that the order in which the parent class method and
         *             the additional tools are defined is important! One does
         *             indeed need to first configure the additional object
         *             based MET significance and AFTER THAT call the parent
         *             class method.
         *
         *             The MET trigger scale factor weights ("MET_TriggerSF")
         *             will be initialized.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initialize); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode initialize();

        /**
         * @brief      Re-implementation of the parent class FillMet method,
         *             calls parent class method. The MET containers
         *
         *             - MetTSTlepInvis
         *
         *             will be filled. These contain the METlepInvis which is
         *             calculated by making the leading muon (incase of CR1 events) or leading two leptons (in case of CR2 ee / mumu events)
         * invisible for the MET calculation. Note that the "conventional" MET containers are still written out, this is done in the
         * XAMPPbase class.
         *
         * @param[in]  systset  The currently active systematic set
         *
         * @return     StatusCode, needs to be checked with ATH_CHECK(FillMet);
         *             to check if the method was successful or failed.
         */
        virtual StatusCode FillMet(const CP::SystematicSet& systset);

        /**
         * @brief      Re-implementation of the parent class SaveScaleFactor
         *             method. First the parent class method is called (which
         *             just returns True and does nothing else at the time of
         *             writing this documentation) and then stores the MET
         *             trigger SF . For retrieving the MET trigger SF either
         *             MetTST or MetTSTlepInvis (m_doMetOneLepton) is used,
         *             depending on the baseline and signal muon multiplicity.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(SaveScaleFactor); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode SaveScaleFactor();

    protected:
        /**
         * @brief      Boolean variable to enable calculation of object based
         *             MET significance for METlepInvis in one lepton control
         *             region.
         */
        bool m_store_significance_lepInvis;

        /**
         * @brief      Missing ET container for MetTST with leptons marked
         *             invisible during MET calculation. Relevant for one-muon and dilepton
         *             control region.
         */
        xAOD::MissingETContainer* m_MetTSTlepInvisible;

        /**
         * @brief      Storage element for MetTrack with leading muon marked
         *             invisible during MET calculation. Relevant for one-muon and dilepton
         *             lepton control region.
         */
        XAMPP::Storage<XAMPPmet>* m_sto_MetTSTlepInvis;

        /**
         * @brief      Pointer to MET significance handler for object-based MET
         *             significance based on MetTSTlepInvisible. Relevant for one-muon and dilepton
         *             lepton control region.
         */
        MetSignificanceHandler_Ptr m_metSignif_noPUJets_noSoftTerm_lepInvis;

        /**
         * @brief      Tool handle for MET trigger scale factor tool
         */
        asg::AnaToolHandle<CP::IMetTriggerSF> m_met_trigger_sf_tool;

        /**
         * @brief      Weights for storing MET trigger scale factors
         */
        std::map<const CP::SystematicSet*, XAMPP::Storage<double>*> m_SFs;
    };
}  // namespace XAMPP

#endif

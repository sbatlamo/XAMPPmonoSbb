#ifndef XAMPPmonoH_MonoHTruthSelector_H
#define XAMPPmonoH_MonoHTruthSelector_H

#include <memory>
#include <utility>
#include <vector>

#include "AthContainers/ConstDataVector.h"
#include "TLorentzVector.h"
#include "XAMPPbase/AnalysisUtils.h"
#include "XAMPPbase/ITruthSelector.h"
#include "XAMPPbase/SUSYTruthSelector.h"

typedef std::vector<TLorentzVector> FourMomentumContainer;

namespace XAMPP {
    class MonoHTruthSelector : public SUSYTruthSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(MonoHTruthSelector, XAMPP::ITruthSelector)

        /**
         * @brief      Default constructor
         *
         * @param[in]  myName  Name of the class instance
         */
        MonoHTruthSelector(std::string myName);

        /**
         * @brief      Default destructor
         */
        virtual ~MonoHTruthSelector() {}

        /**
         * @brief      Re-implements the parent class initialize method. Calls
         *             parent class initialize method, then (if doTruthJets is
         *             enabled) initializes TruthFatJet and TruthTrackJet
         *             containers
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initialize); to check if the
         *             method was successful or failed.
         */
        virtual StatusCode initialize();

        /**
         * @brief      Re-implements the parent class LoadContainers method.
         *             Calls parent class LoadContainers method, then (if
         *             doTruthParticles is enabled) fill W, Z, top containers
         *             with four momenta of the corresponding truth particles.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(LoadContainers); to check if the
         *             method was successful or failed.
         */
        virtual StatusCode LoadContainers();

        /**
         * @brief      Re-implements the parent class InitialFill method. Calls
         *             parent class InitialFill method, then (if doTruthJets is
         *             enabled) fills preFatJets, baselineFatJets, signalFatJets
         *             and preTrackJets, baselineTrackJets, signalTrackJets
         *             containers.
         *
         * @param[in]  systset  The current systematic set
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(InitialFill); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode InitialFill(const CP::SystematicSet& systset);

        /**
         * @brief      Re-implements the parent class FillTruth method. Calls
         *             parent class FillTruth method, then fills the
         *             preselection, baseline and signal containers for
         *             TruthFatJets and TruthTrackJets.
         *
         * @param[in]  systset  The current systematic set
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(FillTruth); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode FillTruth(const CP::SystematicSet& systset);

        /**
         * @brief      Gets the truth large-R jets.
         *
         * @return     Returns pointer to xAOD::JetContainer which contains the truth signal large-R jets
         */
        virtual xAOD::JetContainer* GetTruthFatJets() const { return m_SignalFatJets; }

        /**
         * @brief      Gets the truth W boson momenta.
         *
         * @return     Returns vector of TLorentzVector with truth W boson momenta
         */
        inline virtual const FourMomentumContainer& GetTruthWbosonMomenta() const { return m_truthWbosonMomenta; }

        /**
         * @brief      Gets the truth Z boson momenta.
         *
         * @return     Returns vector of TLorentzVector with truth Z boson momenta
         */
        inline virtual const FourMomentumContainer& GetTruthZbosonMomenta() const { return m_truthZbosonMomenta; }

        /**
         * @brief      Gets the truth top quark momenta.
         *
         * @return     Returns vector of TLorentzVector with truth top quark momenta
         */
        inline virtual const FourMomentumContainer& GetTruthTopQuarkMomenta() const { return m_truthTopQuarkMomenta; }

        /**
         * @brief      Gets the W boson decay modes
         *
         * @param[in]  particle  The truth particle for which the W decay modes should be determined. Before the classification into the
         * different decay modes it is checked whether the particle is a true W boson. If not, then the final decay mode is categorized as
         * WDecayModes::Unclassified.
         *
         * @return     Returns one of the seven decay modes from enum WDecayModes
         */
        virtual int classifyWDecays(const xAOD::TruthParticle* particle);

        /**
         * @brief      Gets the top decay modes
         *
         * @param[in]  particle  The truth particle for which the top decay modes should be determined. Before the classification into the
         * different decay modes it is checked whether the particle is a true top. If not, then the final decay mode is categorized as
         * WDecayModes::Unclassified.
         *
         * @return     Returns one of the seven decay modes from enum WDecayModes, as the top decays are classified by the subsequent W
         * boson decay.
         */
        virtual int classifyTopDecays(const xAOD::TruthParticle* particle);

        /**
         * @brief      Gets the tau decay modes
         *
         * @param[in]  particle  The truth particle for which the tau decay modes should be determined. Before the classification into the
         * different decay modes it is checked whether the particle is a true tau. If not, then the final decay mode is categorized as
         * tauDecayModes::Unclassified.
         *
         * @return     Returns one of the four decay modes from enum tauDecayModes
         */
        virtual int classifyTauDecays(const xAOD::TruthParticle* particle);

        /**
         * @brief      Check if either the top or the antitop decays through a given decay mode
         *
         * @param[in]  topDecay  int variable with a value corresponding to one of the decay modes in enum WDecayModes. Input is the decay
         * mode of the top quark in a ttbar decay.
         * @param[in]  antitopDecay  int variable with a value corresponding to one of the decay modes in enum WDecayModes. Input is the
         * decay mode of the anti-top quark in a ttbar decay.
         * @param[in]  d1  int variable with a value corresponding to one of the decay modes in enum WDecayModes. It is checked whether
         * either the parameter topdecay or the parameter antitopDecay match this given decay mode d1.
         * @param[in]  d2  int variable with a value corresponding to one of the decay modes in enum WDecayModes. It is checked whether
         * either the parameter topdecay or the parameter antitopDecay match this given decay mode d2.
         *
         * @return     Returns True if the ttbar decay proceeds through the given top decay modes d1 and d2.
         */
        virtual bool isCombinedDecay(int topDecay, int antitopDecay, int d1, int d2);

        /**
         * @brief      Gets the reduced W boson decay modes, i.e. the decays W->e+nu and W->mu+nu are summarized into one category. Same
         * holds also for the W decay through a tau, in which the tau decays leptonically.
         *
         * @param[in]  topDecay  int variable with a value corresponding to one of the decay modes in enum WDecayModes.
         *
         * @return     Returns one of the five decay modes from enum reducedWDecayModes
         */
        virtual int getReducedWDecay(int WDecay);

        /**
         * @brief      Gets the ttbar decay modes from a given top and antitop decay mode.
         *
         * @param[in]  reducedtopDecay  int variable with a value corresponding to one of the decay modes in enum reducedWDecayModes. Input
         * is the decay mode of the top quark in a ttbar decay.
         * @param[in]  reducedantitopDecay  int variable with a value corresponding to one of the decay modes in enum reducedWDecayModes.
         * Input is the decay mode of the anti-top quark in a ttbar decay.
         *
         * @return     Returns one of the eleven decay modes from enum ttbarDecayModes
         */
        virtual int combineDecayModes(int reducedtopDecay, int reducedantitopDecay);

        /**
         * @brief      Gets the ttbar decay modes from a given top and anti top quark.
         *
         * @param[in]  top  Input truth particle for the top quark
         * @param[in]  antitop  Input truth particle for the anti-top quark
         *
         * @return     Returns one of the eleven decay modes from enum ttbarDecayModes
         */
        virtual int classifyTtbarDecays(const xAOD::TruthParticle* top, const xAOD::TruthParticle* antitop);

        /**
         * @brief      Possible tau decay modes.
         */
        enum tauDecayModes { TauUnknown = 0, HadTau = 1, ElecTau = 2, MuonTau = 3 };

        /**
         * @brief      Possible W boson decay modes.
         */
        enum WDecayModes { Unspecified = 0, Hadronic = 1, ElecNeut = 2, MuonNeut = 3, HadTauNeut = 4, ElecTauNeut = 5, MuonTauNeut = 6 };

        /**
         * @brief      Possible W boson decay modes. Same as WDecayModes, just that the decays W->e+nu and W->mu+nu are summarized into one
         * category. Same holds also for the W decay through a tau, in which the tau decays leptonically.
         */
        enum reducedWDecayModes { rUnspecified = 0, rHadronic = 1, rLepNeut = 2, rHadTauNeut = 4, rLepTauNeut = 5 };

        /**
         * @brief      Possible ttbar decay modes.
         */
        enum ttbarDecayModes {
            Unknown = 0,
            Lep_Lep = 1,
            Lep_Had = 2,
            Lep_HadTau = 3,
            Lep_LepTau = 4,
            LepTau_Had = 5,
            LepTau_HadTau = 6,
            LepTau_LepTau = 7,
            Had_Had = 8,
            Had_HadTau = 9,
            HadTau_HadTau = 10
        };

    private:
        /**
         * @brief      XAMPP ObjectDefinition for truth large-R jets
         */
        ObjectDefinition m_FatJetDefs;

        /**
         * @brief      Boolean to enable/disable processing of truth large-R
         *             jets.
         *
         * @return     Returns True if truth large-R jets should be processed,
         *             False otherwise
         */
        bool doTruthFatJets() const;

        /**
         * @brief      String storing truth large-R jet container name (if you
         *             don't know this, you can inspect the contents of the xAOD
         *             file to learn the truth large-R jet container name)
         *
         * @return     Returns string storing the truth large-R jet container
         *             name
         */
        std::string FatJetKey() const;

        /**
         * @brief      Jet container for storing truth pre large-R jets
         */
        xAOD::JetContainer* m_PreFatJets;

        /**
         * @brief      Jet container for storing truth baseline large-R jets
         */
        xAOD::JetContainer* m_BaselineFatJets;

        /**
         * @brief      Jet container for storing truth signal large-R jets
         */
        xAOD::JetContainer* m_SignalFatJets;

        /**
         * @brief      XAMPP ObjectDefinition for truth track jets
         */
        ObjectDefinition m_TrackJetDefs;

        /**
         * @brief      Boolean to enable/disable processing of truth track jets.
         *
         * @return     Returns True if truth track jets should be processed,
         *             False otherwise
         */
        bool doTruthTrackJets() const;
        /**
         * @brief      String storing truth track jet container name (if you
         *             don't know this, you can inspect the contents of the xAOD
         *             file to learn the truth track jet container name - note
         *             that not in all xAOD files, e.g. TRUTH3 derivations
         *             there is such an container)
         *
         * @return     Returns string storing the truth track jet container
         *             name
         */
        std::string TrackJetKey() const;
        /**
         * @brief      Pointer to xAOD truth track jet container
         */
        xAOD::JetContainer* m_InitialTrackJets;

        /**
         * @brief      Jet container for storing truth pre track jets
         */
        xAOD::JetContainer* m_PreTrackJets;

        /**
         * @brief      Jet container for storing truth baseline track jets
         */
        xAOD::JetContainer* m_BaselineTrackJets;
        /**
         * @brief      Jet container for storing truth signal track jets
         */
        xAOD::JetContainer* m_SignalTrackJets;

        /**
         * @brief      Helper function for constructing mother particle four
         *             momenta out of primary decay. This function gets the four
         *             momentum of a heavy particle which decayed into a pair of
         *             particles provided by the pdg id pairs. The function
         *             fails if there are ambiguities, e.g. in ZZ->llll events.
         *
         * @param[in]  daughterPdgIdPairs  The daughter pdg identifier pairs
         * @param      motherFourMomenta   The mother four momenta
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(ConstructMotherOfPrimaryDecay); to check if the
         *             method was successful or failed.
         */
        StatusCode ConstructMotherOfPrimaryDecay(const std::vector<std::pair<int, int> >&& daughterPdgIdPairs,
                                                 FourMomentumContainer& motherFourMomenta);

        /**
         * @brief      Helper function to check if truth particle originates
         *             from secondary decay.
         *
         * @param[in]  truthPrt  The truth particle to be checked.
         *
         * @return     True if from secondary decay, False otherwise.
         */
        bool IsFromSecondaryDecay(const xAOD::TruthParticle& truthPrt);

        /**
         * @brief      Helper function to check if truth particle originates
         *             from Bremsstrahlung.
         *
         * @param[in]  truthPrt  The truth particle to be checked.
         *
         * @return     True if brems product, False otherwise.
         */
        bool IsBremsProduct(const xAOD::TruthParticle& truthPrt);

        /**
         * @brief      Container of truth W boson momenta stored as TLorentzVector
         */
        FourMomentumContainer m_truthWbosonMomenta;

        /**
         * @brief      Container of truth Z boson momenta stored as TLorentzVector
         */
        FourMomentumContainer m_truthZbosonMomenta;

        /**
         * @brief      Container of truth top quark momenta stored as TLorentzVector
         */
        FourMomentumContainer m_truthTopQuarkMomenta;
    };

}  // namespace XAMPP
#endif

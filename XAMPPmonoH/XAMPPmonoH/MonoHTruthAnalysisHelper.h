#ifndef MonoHTruthAnalysisHelper_H
#define MonoHTruthAnalysisHelper_H

#include <XAMPPbase/SUSYAnalysisHelper.h>
#include <XAMPPbase/SUSYTruthAnalysisHelper.h>

// Random number generator
#include <TRandom3.h>

namespace XAMPP {
    class MonoHTruthAnalysisHelper : public SUSYTruthAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(MonoHTruthAnalysisHelper, XAMPP::IAnalysisHelper)

        /**
         * @brief      Constructor
         *
         * @param[in]  myname  Name of the class instance.
         */
        MonoHTruthAnalysisHelper(std::string myname);

        /**
         * @brief      Default destructor.
         */
        virtual ~MonoHTruthAnalysisHelper() {}

        /**
         * @brief      Overwrite finalState method of parent class to always always return the value 1.
         *             Note about finalState: SUSY analysis uses this concept to distinguish background: 0 and signal: 1 and higher values.
         *             The FS in cross-section file must also have the value 1 to have the correct cross-section assigned.
         *
         * @return     Returns the final state encoded as integer (background 0, signal 1 or higher value)
         */
        virtual unsigned int finalState();

    protected:
        /**
         * @brief      This method defines all event-level variables that should
         *             be written to the ntuple and initializes them. If you
         *             plan to add a new variable, you need to first initialize
         *             it with
         *
         *             ATH_CHECK(m_XAMPPInfo->NewEventVariable<int/float/double/bool>("name"));
         *
         *             Please note that this will only initialize the storage.
         *             You still need to write the values, for this you must
         *             implement some code in the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method.
         *
         *             In MonoHTruthAnalysisHelper::ComputeEventVariables you need
         *             first to define a XAMPP::Storage object
         *
         *             static XAMPP::Storage<int/float/double/bool>*
         *             dec_variable =
         *             m_XAMPPInfo->GetVariableStorage<int/float/double/bool>("variable");
         *
         *             Then you need to compute the variable and assign it to a
         *             variable of the desired type:
         *
         *             int variable = 42;
         *
         *             Finally you can store it using the storage element:
         *
         *             ATH_CHECK(dec_variable->Store(variable));
         *
         *
         *             You can also initialize storage for particles or jets.
         *             This will store the four-momenta of the particles/jets
         *             and also all decorators that you list. You can do this
         *             with
         *
         *             ATH_CHECK(m_XAMPPInfo->BookParticleStorage("particle",
         *             true));
         *
         *             XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->SaveInteger("some_integer_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveFloat("some_float_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveDouble("some_double_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveChar("some_boolean_value_decorator"));
         *
         *             Note that this will only initialize the storage objects.
         *             You still need to specify what to fill in the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method. There
         *             you also need to define a JetContainer or a
         *             ParticleContainer that should be filled in the particle
         *             storage.
         *
         *             You can add a piece of code to the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method like
         *             this (particle here is a placeholder for either Muon,
         *             Electron, Tau or Jet):
         *
         *             xAOD::ParticleContainer* particles =
         *             m_particle_selection->GetSignalParticles();
         *
         *             static XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->Fill(particles));
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initializeEventVariables); to check if the
         *             method was successful or failed.
         */
        virtual StatusCode initializeEventVariables();

        /**
         * @brief      Calculates the event variables and fills them to the
         *             storage elements defined in
         *             MonoHTruthAnalysisHelper::initializeEventVariables.
         *
         *             You need to first initialize the storage in
         *             MonoHTruthAnalysisHelper::initializeEventVariables by adding a
         *             piece of code to
         *             MonoHTruthAnalysisHelper::initializeEventVariables like this:
         *
         *             ATH_CHECK(m_XAMPPInfo->NewEventVariable<int/float/double/bool>("name"));
         *
         *             Then you can compute the event level variable in the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method and
         *             store the result.
         *
         *             Here is a generic example, how to calculate and store
         *             event level variables:
         *
         *             First you need to define a XAMPP::Storage element to
         *             access the storage defined in
         *             MonoHTruthAnalysisHelper::initializeEventVariables.
         *
         *             static XAMPP::Storage<int/float/double/bool>*
         *             dec_variable =
         *             m_XAMPPInfo->GetVariableStorage<int/float/double/bool>("variable");
         *
         *             Then you need to compute the variable and assign it to a
         *             variable of the desired type:
         *
         *             int variable = 42;  // just an example, in reality you
         *             would compute here e.g. the angular separation between
         *             two jets or retrieve a tagger score.
         *
         *             Finally you can store it using the storage element
         *             defined earlier.
         *
         *             ATH_CHECK(dec_variable->Store(variable));
         *
         *
         *             It is also possible to write the four-vector content of
         *             particle or jet containers with selected decorator
         *             information to the ntuples. This will store the
         *             four-momenta of the particles/jets and also all
         *             decorators that you list. Before you can do this, you
         *             need to initialize in
         *             MonoHTruthAnalysisHelper::initializeEventVariables the storage
         *             with with
         *
         *             ATH_CHECK(m_XAMPPInfo->BookParticleStorage("particle",
         *             true));
         *
         *             XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->SaveInteger("some_integer_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveFloat("some_float_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveDouble("some_double_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveChar("some_boolean_value_decorator"));
         *
         *             Note that this will only initialize the storage objects.
         *
         *             You then can specify what to fill in the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method. Here
         *             you also need to define a JetContainer or a
         *             ParticleContainer that should be filled in the particle
         *             storage.
         *
         *             You can add a piece of code to the
         *             MonoHTruthAnalysisHelper::ComputeEventVariables method like
         *             this (particle here is a placeholder for either Muon,
         *             Electron, Tau or Jet):
         *
         *             xAOD::ParticleContainer* particles =
         *             m_particle_selection->GetSignalParticles();
         *
         *             static XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->Fill(particles));
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(ComputeEventVariables); to check if the method
         *             was successful or failed.
         */
        virtual StatusCode ComputeEventVariables();

        virtual bool isBJet(const xAOD::Jet& jet) const;

    private:
        /**
         * @brief      Random number generator (TRandom3 reference:
         *             https://root.cern.ch/doc/master/classTRandom3.html) used
         *             for emulating b-tagging efficiency on truth level.
         */
        std::unique_ptr<TRandom3> m_r3;
    };
}  // namespace XAMPP

#endif

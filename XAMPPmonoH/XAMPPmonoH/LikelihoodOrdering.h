#ifndef XAMPPmonoH_LikelihoodOrdering_H
#define XAMPPmonoH_LikelihoodOrdering_H

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TMath.h"
#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"

namespace XAMPP {

    enum NLepton { ZeroLepton = 0, OneLepton, TwoLepton };

    /**
     * @brief      This function returns the z-component of pTV.
     *
     *             It is required for the likelihood calculation of the
     *             likelihood ordering to reduce ttbar in the resolved signal
     *             region.
     *
     *             In the 0 lepton region, the lepton is mostly a taujet. One
     *             uses invariant mass of leptonic W to find z-component of pTV:
     *             (pTV + tau)^2 = m_W^2 --> solve quadratic equation in pTV_z.
     *             pTV_z is used for invariant mass of leptonic top in
     *             likelihood function: top_lep = W_lep + b_lep
     *
     * @param[in]  tauInPermutation  The TLorentzVector of a lepton (most likely
     *                               to be a tau, hence the name)
     * @param[in]  metVec            The MET as TLorentzVector
     * @param[in]  type              The type of the permutation (used in: METz
     *                               = 1000 * (center + type *
     *                               TMath::Sqrt(discriminant) / (energyTau *
     *                               energyTau - pTauZ * pTauZ));)
     *
     * @return     The z-component of pTV calculated from lepton and MET as
     *             float
     */
    float METz(TLorentzVector tauInPermutation, TLorentzVector metVec, int type);

    /**
     * @brief      This function returns -2 times the logarithm of a
     *             Breit-Wigner function: 2 * ln( (mass^2 - poleMass^2)^2 +
     *             (poleMass * width)^2 );
     *             (https://en.wikipedia.org/wiki/Cauchy_distribution)
     *
     *             It is required for the likelihood calculation of the
     *             likelihood ordering to reduce ttbar in the resolved signal
     *             region.
     *
     * @param[in]  mass      The mass
     * @param[in]  poleMass  The pole mass (e.g. of the W boson or the top
     *                       quark)
     * @param[in]  width     The width
     *
     * @return     The float value of -2 times the logarithm of a Breit-Wigner
     *             function
     */
    float LBW(float mass, float poleMass, float width);

    /**
     * @brief      This function returns -2ln(exponential), which is the
     *             Chi2-term required for the likelihood calculation
     *
     *             It is required for the likelihood calculation of the
     *             likelihood ordering to reduce ttbar in the resolved signal
     *             region.
     *
     * @param[in]  mass      The mass of the resonance
     * @param[in]  poleMass  The pole mass (e.g. of the W boson or the top
     *                       quark)
     * @param[in]  width     The width of the resonance
     *
     * @return     Returns float value of -2ln(exponential)
     */
    float LChi2(float mass, float poleMass, float width);

    /**
     * @brief      This function returns the value used for an elliptic cut
     *             which is used to reject events inside the ellipse. The
     *             parameters of the ellipse are optimised based on a study and
     *             are hard-coded in the function implementation. The ellipse is
     *             defined by
     *
     *              - (x_0,y_0), the center of ellipse,
     *              - theta, the rotation angle in RAD
     *              - a,b, the major and minor semi-axis of the ellipse
     *
     * @param[in]  mtop  The top mass (x axis of ellipse)
     * @param[in]  mW    The W mass (y axis of ellipse)
     * @param[in]  btag  The b-tag multiplicity
     *
     * @return     Returns float value to be used for elliptic cut.
     */
    float EllipticCut(float mtop, float mW, int btag);

    /**
     * @brief      Calculates the likelihood of a given jet permutation and
     *             returns -2 times ln(Likelihood) of given parton-jet permutation.
     *
     * @param      measuredMasses  The measured masses entering the likelihood
     *                             calculation
     * @param[in]  Njets           The number of jets in the given parton-jet
     *                             permutation
     *
     * @return     Returns -2 times ln(Likelihood) of given parton-jet permutation.
     */
    float CalculateLikelihood(std::vector<float>& measuredMasses, int Njets);

    /**
     * @brief      Function for likelihood ordering for exactly 4 jets. This
     *             shall reduce ttbar in the resolved signal region. When one
     *             considers that most of ttbar has one leptonic decaying W
     *             boson.
     *
     * @param[in]  jetsForHbbReco   The jets for hbb reco
     * @param[in]  tlv_complete_j0  The TLorentzVector of first jet from hadronic W decay
     * @param[in]  tlv_complete_j1  The TLorentzVector of second jet from hadronic W decay
     * @param[in]  btag             The btag multiplicity
     *
     * @return     Returns float value for likelihood ordering for ttbar rejection.
     */
    float LikelihoodOrderingFourJets(std::vector<const xAOD::Jet*> jetsForHbbReco, TLorentzVector tlv_complete_j0,
                                     TLorentzVector tlv_complete_j1, int btag);

    /**
     * @brief      Function for likelihood ordering. This shall reduce ttbar in
     *             the resolved signal region. When one considers that most of
     *             ttbar has one leptonic decaying W boson.
     *
     * @param[in]  jetsForHbbReco   The jets for hbb reco
     * @param[in]  tlv_complete_j0  The TLorentzVector of first jet from
     *                              hadronic W decay
     * @param[in]  tlv_complete_j1  The TLorentzVector of second jet from
     *                              hadronic W decay
     * @param[in]  metVec           The met vector
     * @param[in]  muon             The muon
     * @param[in]  btag             The btag multiplicity
     * @param[in]  nlep             The lepton multiplicity
     *
     * @return     Returns float value for likelihood ordering for ttbar
     *             rejection.
     */
    float LikelihoodOrdering(std::vector<const xAOD::Jet*> jetsForHbbReco, TLorentzVector tlv_complete_j0, TLorentzVector tlv_complete_j1,
                             TLorentzVector metVec, const xAOD::Muon* muon, int btag, NLepton nlep);
}  // namespace XAMPP
#endif

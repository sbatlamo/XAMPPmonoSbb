#ifndef XAMPPMONOH_XAMPPMONOHWZDICT_H
#define XAMPPMONOH_XAMPPMONOHWZDICT_H

#include <XAMPPmonoH/MonoHAnalysisConfig.h>
#include <XAMPPmonoH/MonoHAnalysisHelper.h>
#include <XAMPPmonoH/MonoHJetSelector.h>
#include <XAMPPmonoH/MonoHMetSelector.h>
#include <XAMPPmonoH/MonoHTruthSelector.h>

#endif

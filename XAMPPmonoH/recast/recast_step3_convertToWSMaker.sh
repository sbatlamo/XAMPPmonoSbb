#!/bin/bash

##############################
# Setup                      #
##############################
# prepare environment (assuming this script is located in <basedir>/XAMPPmonoH/recast)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh
cd ${BASEDIR}

TESTDIR=${BASEDIR}/output_recast

###########################################################
# Convert XAMPPplotting output to WSMaker histogram input #
###########################################################
rm -rf ${TESTDIR}/recast_histograms
mkdir ${TESTDIR}/recast_histograms 
hadd ${TESTDIR}/recast_histograms/signal.root ${TESTDIR}/recast_signal.root


python ${BASEDIR}/XAMPPmonoH/python/XAMPPplottingToWSMaker.py -i ${TESTDIR}/recast_histograms -o ${TESTDIR}/recast_signal_WSMaker.root -l 79.80811635975


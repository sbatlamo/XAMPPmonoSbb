#include <XAMPPmonoH/JetTruthAssociationModule.h>
#include <XAMPPmonoH/MonoHAnalysisConfig.h>
#include <XAMPPmonoH/MonoHAnalysisHelper.h>
#include <XAMPPmonoH/MonoHJetSelector.h>
#include <XAMPPmonoH/MonoHMetSelector.h>
#include <XAMPPmonoH/MonoHTruthAnalysisHelper.h>
#include <XAMPPmonoH/MonoHTruthSelector.h>

#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHAnalysisHelper)
DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHAnalysisConfig)

DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHTruthAnalysisHelper)
DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHTruthAnalysisConfig)

DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHTruthSelector)
DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHJetSelector)
DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, MonoHMetSelector)

DECLARE_NAMESPACE_TOOL_FACTORY(XAMPP, JetTruthAssociationModule)

#include "../DecorateLRTruthLabels.h"
DECLARE_ALGORITHM_FACTORY(DecorateLRTruthLabels)

#include "../TransferAssociatedVRJets.h"
DECLARE_ALGORITHM_FACTORY(TransferAssociatedVRJets)

DECLARE_FACTORY_ENTRIES(XAMPPmonoH) {
    DECLARE_ALGORITHM(DecorateLRTruthLabels);
    DECLARE_ALGORITHM(TransferAssociatedVRJets)
    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHAnalysisHelper)
    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHAnalysisConfig)

    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHTruthAnalysisHelper)
    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHTruthAnalysisConfig)

    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHTruthSelector)
    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHJetSelector)
    DECLARE_NAMESPACE_TOOL(XAMPP, MonoHMetSelector)
    DECLARE_NAMESPACE_TOOL(XAMPP, JetTruthAssociationModule)
}

#include "TransferAssociatedVRJets.h"

#include <algorithm>
#include <iterator>

TransferAssociatedVRJets::TransferAssociatedVRJets(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAnalysisAlgorithm(name, pSvcLocator) {
    declareProperty("InputJets", m_inputJets);
    declareProperty("InputLinks", m_inputLinkName);
    declareProperty("OutputLinks", m_outputLinkName);
    declareProperty("NewLinkedContainer", m_newLinkedContainer);
}

TransferAssociatedVRJets::~TransferAssociatedVRJets() {}

StatusCode TransferAssociatedVRJets::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");
    m_inputLink.emplace(m_inputLinkName);
    m_outputLink.emplace(m_outputLinkName);
    return StatusCode::SUCCESS;
}

StatusCode TransferAssociatedVRJets::execute() {
    ATH_MSG_DEBUG("Executing " << name() << "...");
    const xAOD::IParticleContainer* jets = nullptr;
    ATH_CHECK(evtStore()->retrieve(jets, m_inputJets));
    const xAOD::IParticleContainer* target = nullptr;
    ATH_CHECK(evtStore()->retrieve(target, m_newLinkedContainer));
    for (const xAOD::IParticle* ipart : *jets) {
        const veclink_t& inputLinks = (*m_inputLink)(*ipart);
        veclink_t& outputLinks = (*m_outputLink)(*ipart);
        outputLinks.reserve(inputLinks.size());
        std::transform(inputLinks.begin(), inputLinks.end(), std::back_inserter(outputLinks),
                       [target](const link_t& link) { return link_t(*target, link.index()); });
    }

    return StatusCode::SUCCESS;
}

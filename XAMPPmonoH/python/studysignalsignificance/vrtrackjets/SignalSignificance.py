#!/usr/bin/env python

from argparse import ArgumentParser
parser = ArgumentParser(description='calculate significances over signal grid')
parser.add_argument("-b", "--BackgroundHistos", help="give path to background histograms", default="")
parser.add_argument("-s", "--SignalHistos", help="give path to signal histograms", default="")
parser.add_argument("-o", "--Output", help="Where output should be stored", default="significances.pdf")
parser.add_argument("-l", "--Lumi", help="Luminosity", type=float, default=80.)
parser.add_argument("--Debug", help="Debug mode: Print yields and signficances", action='store_true', default=False)
parser.add_argument("--Yields", help="Plot yields instead of signficances", action='store_true', default=False)
args = parser.parse_args()

from ROOT import *
import os, math

gROOT.LoadMacro("~/RootUtils/AtlasStyle.C")
gROOT.ProcessLine("SetAtlasStyle()")
gStyle.SetPalette(1)

# input directories containing signal and background histograms
Inputdir = args.BackgroundHistos
Inputdir_sig = args.SignalHistos

tags = ["1b", "2b", "all"]
regions = ["merged", "HiggsWindow"]

# labels on the plots
label = {}
for tag in tags:
    label[tag] = {}
label["1b"]["merged"] = "1 b-tag"
label["1b"]["HiggsWindow"] = "1 b-tag, 70 GeV < m_{J} < 140 GeV"
label["2b"]["merged"] = "2 b-tags"
label["2b"]["HiggsWindow"] = "2 b-tags, 70 GeV < m_{J} < 140 GeV"
label["all"]["merged"] = "1+2 b-tags"
label["all"]["HiggsWindow"] = "1+2 b-tags, 70 GeV < m_{J} < 140 GeV"


# get signal name from histo file
# assuming naming like:
# /project/etp5/amatic/MonoH/XAMPP_Histograms_SR_VRsignal_03-05-2018/zp2hdm_bb_mzp2400_mA300_SR_Merged.root
def get_signame(sigfile):
    signame_root = sigfile.split("/")[-1]
    signame = signame_root.replace(".root", "")
    return signame


# extract Z' mass from signal name
def get_mZp(sigfile):
    signame = get_signame(sigfile)
    mZp_block = signame.split("_")[2]
    mZp = mZp_block.replace("mzp", "")
    return int(mZp)


# extract A mass from signal name
def get_mA(sigfile):
    signame = get_signame(sigfile)
    mA_block = signame.split("_")[3]
    mA = mA_block.replace("mA", "")
    return int(mA)


# calculation of significance using formula from support note
def getSignificance(sigYield, bkgYield):
    significance = math.sqrt(2 * ((sigYield + bkgYield) * math.log(1 + (sigYield / bkgYield)) - sigYield))
    signficance_rounded = round(significance, 2)
    return signficance_rounded


def SetAtlasLabel():
    ATLAS_label = TLatex()
    ATLAS_label.SetNDC()
    ATLAS_label.SetTextFont(43)
    ATLAS_label.SetTextSize(19)
    ATLAS_label.SetTextColor(kBlack)
    ATLAS_label.SetTextAlign(11)
    ATLAS_label.DrawLatex(0.15, 0.94, "#font[72]{ATLAS} Internal      #sqrt{s} = 13 TeV, " + str(args.Lumi) + " fb^{-1}")


def SetRegionLabel(text):
    region_label = TLatex()
    region_label.SetNDC()
    region_label.SetTextFont(43)
    region_label.SetTextSize(18)
    region_label.SetTextColor(kBlack)
    region_label.DrawLatex(0.15, 0.88, text)


def main():

    # sorting MC signals and backgrounds
    # (necessary if other files, e.g. data, in input directories)
    backgrounds = []
    signals = []
    for infile in os.listdir(Inputdir):
        if not (infile.startswith("data") or infile.startswith("zp2hdm")):
            backgrounds.append(os.path.join(Inputdir, infile))
    for infile in os.listdir(Inputdir_sig):
        if (infile.startswith("zp2hdm")):
            signals.append(os.path.join(Inputdir_sig, infile))
    print backgrounds
    # Defining dictionaries needed later in significance calculation and plotting
    integral_bkg = {}
    integral_bkg_tot = {}
    m_J_histo_bkg = {}
    significance = {}
    histo_significance = {}
    root_histo = {}
    c = {}
    for tag in tags:
        integral_bkg[tag] = {}
        integral_bkg_tot[tag] = {}
        significance[tag] = {}
        histo_significance[tag] = {}
        root_histo[tag] = {}
        c[tag] = {}
        for reg in regions:
            integral_bkg_tot[tag][reg] = 0.
            histoname = "h_sig_" + tag + "_" + reg
            histo_significance[tag][reg] = TH2F(histoname, histoname, 14, 300., 3100, 7, 150., 850.)

    # looping over background histograms to calculate SR event yield (in full SR merged as well as in Higgs mass window only)
    for bkg in backgrounds:
        # get the m_J histograms for each b-tag multiplicity
        rootfile = TFile.Open(bkg)
        #m_J_histo_bkg["1b"] = rootfile.Get("MonoH_Nominal/SR_Merged_1b/fatjets_m1_10GeV")
        #m_J_histo_bkg["2b"] = rootfile.Get("MonoH_Nominal/SR_Merged_2b/fatjets_m1_10GeV")
        m_J_histo_bkg["1b"] = rootfile.Get("m_J_1b")
        m_J_histo_bkg["2b"] = rootfile.Get("m_J_2b")
        # Get binnumbers for the edges of the Higgs mass window
        # (enough to do it for only a single b-tag multiplicity, as the binning of the histograms is the same)
        binHiggs_70 = m_J_histo_bkg["1b"].GetXaxis().FindBin(70)
        binHiggs_140 = m_J_histo_bkg["1b"].GetXaxis().FindBin(140)

        # integrate m_J histogram: either full histogram or only Higss mass Window
        for tag in tags:
            if not (tag == "all"):
                integral_bkg[tag]["merged"] = m_J_histo_bkg[tag].Integral()
                integral_bkg_tot[tag]["merged"] += integral_bkg[tag]["merged"]
                integral_bkg[tag]["HiggsWindow"] = m_J_histo_bkg[tag].Integral(binHiggs_70, binHiggs_140)
                integral_bkg_tot[tag]["HiggsWindow"] += integral_bkg[tag]["HiggsWindow"]

    #scale the background yields to the luminosity
    for tag in tags:
        for reg in regions:
            integral_bkg_tot[tag][reg] = args.Lumi * integral_bkg_tot[tag][reg]

    for sig in signals:
        m_J_histo_sig = {}
        if args.Debug:
            print "*************"
            print sig
            print "mZp = ", get_mZp(sig)
            print "mA = ", get_mA(sig)
        rootfile = TFile.Open(sig)
        m_J_histo_sig["1b"] = rootfile.Get("MonoH_Nominal/SR_Merged_1b/fatjets_m1_10GeV")
        m_J_histo_sig["2b"] = rootfile.Get("MonoH_Nominal/SR_Merged_2b/fatjets_m1_10GeV")
        #m_J_histo_sig["1b"] = rootfile.Get("m_J_1b")
        #m_J_histo_sig["2b"] = rootfile.Get("m_J_2b")
        # assign correct bin numbers to grid points
        binx = (get_mZp(sig) - 200) / 200
        biny = (get_mA(sig) - 100) / 100
        integral_sig = {}
        binHiggsSig_70 = m_J_histo_sig["1b"].GetXaxis().FindBin(70)
        binHiggsSig_140 = m_J_histo_sig["1b"].GetXaxis().FindBin(140)
        for tag in tags:
            if not (tag == "all"):
                if args.Debug:
                    print "tag = ", tag
                integral_sig[tag] = {}
                integral_sig[tag]["merged"] = m_J_histo_sig[tag].Integral()
                integral_sig[tag]["HiggsWindow"] = m_J_histo_sig[tag].Integral(binHiggsSig_70, binHiggsSig_140)
                for reg in regions:
                    integral_sig[tag][reg] = args.Lumi * integral_sig[tag][reg]
                    significance[tag][reg] = getSignificance(integral_sig[tag][reg], integral_bkg_tot[tag][reg])
                    if not args.Yields:
                        # 2D histogram showing significance on z-Axis
                        histo_significance[tag][reg].SetBinContent(binx, biny, significance[tag][reg])
                    else:
                        # 2D histogram showing yields on z-Axis
                        histo_significance[tag][reg].SetBinContent(binx, biny, round(integral_sig[tag][reg], 2))
                    if args.Debug:
                        print "region = ", reg
                        print "integral_bkg_tot = ", integral_bkg_tot[tag][reg]
                        print "integral_sig = ", integral_sig[tag][reg]
                        print "significance = ", significance[tag][reg]
        # calculate significance for combination of 1b and 2b SR
        # using approximation: Z_all = sqrt(Z1^2 + Z2^2)
        for reg in regions:
            significance["all"][reg] = round(math.sqrt((significance["1b"][reg])**2 + (significance["2b"][reg])**2), 2)
            if not args.Yields:
                histo_significance["all"][reg].SetBinContent(binx, biny, significance["all"][reg])
            else:
                histo_significance["all"][reg].SetBinContent(binx, biny, round(integral_sig["1b"][reg] + integral_sig["2b"][reg], 2))

    #create output directory
    if not os.path.exists(args.Output):
        os.makedirs(args.Output)

    # plot the significances
    histo_significance_all = {}
    for reg in regions:
        for tag in tags:
            c[tag][reg] = TCanvas()
            if not args.Yields:
                root_histo[tag][reg] = TFile(os.path.join(args.Output, "significance_" + tag + "_" + reg + ".root"), "RECREATE")
                outName = "significance_" + tag + "_" + reg + ".pdf"
                histo_significance[tag][reg].GetZaxis().SetTitle("Significance Z")
            else:
                root_histo[tag][reg] = TFile(os.path.join(args.Output, "yields_" + tag + "_" + reg + ".root"), "RECREATE")
                histo_significance[tag][reg].GetZaxis().SetTitle("Signal yields")
                outName = "yields_" + tag + "_" + reg + ".pdf"
            histo_significance[tag][reg].GetXaxis().SetTitle("m_{Z'} [GeV]")
            histo_significance[tag][reg].GetYaxis().SetTitle("m_{A} [GeV]")
            histo_significance[tag][reg].Draw("COLZ TEXT")
            histo_significance[tag][reg].Write()
            root_histo[tag][reg].Close()
            SetAtlasLabel()
            SetRegionLabel(label[tag][reg])
            c[tag][reg].SetRightMargin(0.2)
            c[tag][reg].SetLeftMargin(0.15)
            c[tag][reg].SetTopMargin(0.15)
            outPath = os.path.join(args.Output, outName)
            c[tag][reg].Print(outPath)
            c[tag][reg].Close()


if __name__ == "__main__":
    main()

#!/usr/bin/env python
import os, sys, random
import json
import subprocess
import itertools
from argparse import ArgumentParser
from datetime import datetime
from pprint import pprint, pformat
from ClusterSubmission.Utils import ResolvePath, CreateDirectory
from XAMPPmonoH.createInputConfig import InputConfigMaker
import XAMPPmonoH.createDSConfig
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


class PlottingControl(object):
    """Class for controling XAMPPplotting for the mono-h(bb) analysis"""
    def __init__(self, configPath):
        super(PlottingControl, self).__init__()
        self.inputBasePath = ''
        self.outputBasePath = ''
        # regions
        self.regions = []
        # samples to be processed for MC (possible choices: "data15", "data16", "ttbar", ...)
        self.samples = []
        # signals to be processed for MC (need only to specify part of the name, e.g. "zp2hdm")
        self.signals = []
        # select which MC campaigns will be processed (agreement with the data campaigns which have to be specified as samples is checked automatically)
        self.campaigns = []
        self.variables = []
        self.format = ['eps']
        self.doLogY = False
        self.LowerPadRanges = []
        # tag of ntuple production
        self.tag = "00-00-00"
        # path to local storage of ntuples (absolute path)
        self.ntuplesPath = {}
        # batch engine / local processing
        self.engine = "LOCAL"
        # luminosity for creating plots (can be either a floating point number or the string "auto" to calculate the luminosity from the data input configs)
        self.luminosity = "auto"

        self.doBlinding = True
        self.doFitInputs = False
        self.doSystematics = False
        self.doPRW = True
        self.skipInputConfigCreation = False

        # internal variables
        self._campaigns = {'data15': 'mc16a', 'data16': 'mc16a', 'data17': 'mc16d', 'data18': 'mc16e'}
        self._enforceBlinding = ['data18']
        self._runconfigbase = "XAMPPplotting/RunConf/MonoH"
        self._regions = {
            'SR': [
                os.path.join(self._runconfigbase, "RunConfig_SR_Merged.conf"),
                os.path.join(self._runconfigbase, "RunConfig_SR_Resolved.conf")
            ],
            'CR1': [
                os.path.join(self._runconfigbase, "RunConfig_CR1_Merged.conf"),
                os.path.join(self._runconfigbase, "RunConfig_CR1_Resolved.conf")
            ],
            'CR2ee': [
                os.path.join(self._runconfigbase, "RunConfig_CR2_Merged_ee.conf"),
                os.path.join(self._runconfigbase, "RunConfig_CR2_Resolved_ee.conf")
            ],
            'CR2mumu': [
                os.path.join(self._runconfigbase, "RunConfig_CR2_Merged_mumu.conf"),
                os.path.join(self._runconfigbase, "RunConfig_CR2_Resolved_mumu.conf")
            ],
            'PH': [],
            'TRUTH': [
                os.path.join(self._runconfigbase, "Truth", "RunConfig_Truth_SR_Merged.conf"),
                os.path.join(self._runconfigbase, "Truth", "RunConfig_Truth_SR_Resolved.conf")
            ]
        }
        self._histogrambase = 'XAMPPplotting/HistoConf/MonoH'
        self._histograms = {
            'SR': os.path.join(self._histogrambase, "Histos_SR.conf"),
            'CR1': os.path.join(self._histogrambase, "Histos_CR1.conf"),
            'CR2ee': os.path.join(self._histogrambase, "Histos_CR2.conf"),
            'CR2mumu': os.path.join(self._histogrambase, "Histos_CR2.conf"),
            'PH': "",
            'TRUTH': os.path.join(self._histogrambase, "Histos_Truth.conf"),
            "Fitting_SR": os.path.join(self._histogrambase, "Histos_Fitting.conf"),
            "Fitting_CR1": os.path.join(self._histogrambase, "Histos_Fitting1L.conf"),
            "Fitting_CR2ee": os.path.join(self._histogrambase, "Histos_Fitting2L.conf"),
            "Fitting_CR2mumu": os.path.join(self._histogrambase, "Histos_Fitting2L.conf")
        }
        self._process = set()
        self._jobname = 'analysis'

        self.readConfig(configPath)
        self.checkCampaigns()
        self.checkRegions()
        self.checkConfiguration()

    def checkCampaigns(self):
        """Check configuration and add data / mc campaigns to internal set for the to-do list which campaigns to process."""
        # if data15 is included in data check if also data16 is included and vice versa
        if 'data15' in self.samples and 'data16' not in self.samples:
            logging.warning(
                "data15 is included in data campaign list but data16 is missing. Those should be both used together with mc16a.")
        if 'data16' in self.samples and 'data15' not in self.samples:
            logging.warning(
                "data16 is included in data campaign list but data15 is missing. Those should be both used together with mc16a.")

        # check if each data has its mc campaign and ask user if he/she/they did not forget something
        for campaign in self.samples:
            # only check data samples
            if not 'data' in campaign: continue
            if campaign not in self._campaigns.keys():
                logging.warning("Data sample name {data} not allowed. Possible choices: ".format(data=campaign) +
                                pformat(sorted(self._campaigns.keys())))
            elif self._campaigns[campaign] not in self.campaigns:
                print(campaign)
                print(self._campaigns.keys())
                print(self._campaigns.values())
                print(self.campaigns)
                logging.warning(
                    "Data sample name {data} is included in sample list but corresponding MC campaign {mc} is not included in campaigns!".
                    format(data=campaign, mc=self._campaigns[campaign]))
            else:
                self._process.add(campaign)
        for campaign in self.campaigns:
            if campaign not in self._campaigns.values():
                logging.warning("MC campaign {mc} not an allowed campaign. Possible choices: ".format(mc=campaign) +
                                pformat(sorted(self._campaigns.values())))
            else:
                self._process.add(campaign)

    def checkRegions(self):
        """Check if regions selected in config file comply with regions allowed by script."""
        for region in self.regions:
            if region not in self._regions.keys():
                logging.warning("region {region} not an allowed region. Possible choices: ".format(region=region) +
                                pformat(sorted(self._regions.keys())))

    def checkConfiguration(self):
        """Check if the configuration makes sense."""
        if self.doFitInputs and not self.doSystematics and not self.doPRW:
            logging.warning("Check that you really want these settings for creating fit inputs:")
            logging.warning("- doFitInputs: {t}".format(t="True" if self.doFitInputs else "False"))
            logging.warning("- doSystematics: {t}".format(t="True" if self.doSystematics else "False"))
            logging.warning("- doPRW: {t}".format(t="True" if self.doPRW else "False"))

        if self.doFitInputs and self.doSystematics and self.engine == 'LOCAL':
            logging.warning(
                "Attention! You selected to produce fit inputs with systematics with the local engine. This can take VERY long. It is recommended to use a batch system instead."
            )

    def readConfig(self, configPath):
        """Read configuration file formated in json to extract information to fill TemplateMaker variables."""
        try:
            stream = open(configPath, 'r')
            config = json.loads(stream.read())
            member_variables = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("_")]
            # check if member variable name is in config file. if it is, update member variable to value of config file
            for member in config.keys():
                if member in member_variables:
                    setattr(self, member, config[member])
        except Exception as e:
            logging.error("Error reading configuration '{config}'".format(config=configPath))
            logging.error(e)

    def modifyLine(self, runconfigfile, configline, switchOn, value=''):
        """Switch line in config file on/off by commeting it in our out or replacing it by a specific value if the latter is specified."""
        temp = ""
        with open(runconfigfile) as inFile:
            for line in inFile:
                if configline.strip() in line:
                    if value:
                        temp += (configline + ' ' + value) + '\n'
                    else:
                        temp += (configline if switchOn else '# ' + configline) + '\n'
                else:
                    temp += line
        logging.debug("Writing updated run config file")
        logging.debug(temp)
        with open(runconfigfile, 'w') as outFile:
            outFile.write(temp)

    def ensureBlinding(self, region):
        """Make sure that blinding is applied correctly to the signal regions."""
        doBlinding = self.doBlinding
        for campaign in self.samples:
            if 'data' not in campaign: continue
            if campaign in self._enforceBlinding and not self.doBlinding and "data" in self.samples:
                logging.warning("Not allowed to unblind {data18}! Enforcing blinding.")
                doBlinding = True
        # do not blind control regions
        if region != 'SR':
            doBlinding = False

        # merged signal region
        blindingfile = ResolvePath("XAMPPplotting/RunConf/MonoH/Common/RunConfig_common_SR_Merged.conf")
        blindingline = '    @Blinding_Merged'
        self.modifyLine(blindingfile, blindingline, doBlinding)
        # resolved signal region
        blindingfile = ResolvePath("XAMPPplotting/RunConf/MonoH/Common/RunConfig_common_SR_Resolved.conf")
        blindingline = '    @Blinding_Resolved'
        self.modifyLine(blindingfile, blindingline, doBlinding)

    def customiseRunConfigs(self):
        """Modify run config according to settings."""
        runconfigfile = ResolvePath("XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf")

        # normalize to period
        runconfigline = 'normalizeToPeriod'
        value = 'all'
        if 'mc16a' in self.campaigns and 'mc16d' not in self.campaigns and 'mc16e' not in self.campaigns:
            value = 'data15-16'
        elif 'mc16a' not in self.campaigns and 'mc16d' in self.campaigns and 'mc16e' not in self.campaigns:
            value = 'data17'
        elif 'mc16a' not in self.campaigns and 'mc16d' not in self.campaigns and 'mc16e' in self.campaigns:
            value = 'data18'
        elif 'mc16a' in self.campaigns and 'mc16d' in self.campaigns and 'mc16e' not in self.campaigns:
            value = 'data15_17'
        elif 'mc16a' not in self.campaigns and 'mc16d' in self.campaigns and 'mc16e' in self.campaigns:
            value = 'data17_18'
        elif 'mc16a' in self.campaigns and 'mc16d' not in self.campaigns and 'mc16e' in self.campaigns:
            value = 'data15_18'
        self.modifyLine(runconfigfile, runconfigline, True, value)
        # PRW
        runconfigline = 'noPRW'
        self.modifyLine(runconfigfile, runconfigline, not self.doPRW)

        # systematics
        runconfigline = 'noSyst'
        self.modifyLine(runconfigfile, runconfigline, not self.doSystematics)

    def createInputConfig(self, inputDir, outputDir, campaigns=[]):
        """Create input config on-the-fly for XAMPPplotting histogram creation."""
        excludedSamples = []
        CreateDirectory(outputDir, False)
        configpath = ResolvePath('XAMPPmonoH/inputConfig_config.json')
        maker = InputConfigMaker(configpath, False)
        maker.addCampaigns(campaigns)
        maker.getFiles(inputDir)
        maker.makeConfigs(excludedSamples, outputDir, excludeExtensions=False)
        return outputDir

    def createDSConfig(self, histogramDir, lumi=1., inputconfigs=[]):
        """Create DS config on-the-fly for XAMPPplotting plot creation."""
        DSConfig = os.path.join(histogramDir, 'MonoH_DSConfig.py')
        configpath = ResolvePath('XAMPPmonoH/DSConfig_config.json')
        samples_mc = XAMPPmonoH.createDSConfig.readConfig(configpath)
        inputs_data, inputs_mc = XAMPPmonoH.createDSConfig.scanDirectory(histogramDir)
        XAMPPmonoH.createDSConfig.writeDSConfig(DSConfig,
                                                histogramDir,
                                                inputs_data,
                                                inputs_mc,
                                                samples_mc,
                                                lumi=lumi,
                                                inputconfigs=inputconfigs)
        return DSConfig

    def getInputConfigDirNames(self, region):
        """Get default name for input config directory."""
        assert region in self._regions.keys()

        inputConfigNames = {}
        for campaign in self.campaigns:
            # different input config folders for combinations of campaigns
            if 'mc16a' in campaign:
                substring = 'data1516mc16a'
            elif 'mc16d' in campaign:
                substring = 'data17mc16d'
            elif 'mc16e' in campaign:
                substring = 'data18mc16e'
            elif 'all' in campaign:
                substring = ''
            elif 'truth' in campaign:
                substring = ''

            name = "XAMPPmonoH-" + self.tag + '.' + (substring + '.' if substring else '') + region
            inputConfigNames[campaign] = os.path.join(self.inputBasePath, name)
        return inputConfigNames

    def processRegion(self, region):
        """Process region and create plots + histograms."""
        # ensure use of correct blinding strategy
        self.ensureBlinding(region)

        # input configs for histogram creation with XAMPPplotting
        inputConfigs = []
        inputConfigNames = self.getInputConfigDirNames(region)
        for campaign, inputConfig in inputConfigNames.items():
            if not self.skipInputConfigCreation:
                self.createInputConfig(self.ntuplesPath[region], inputConfig, [campaign])
            for config in os.listdir(inputConfig):
                # if user specified samples and signals in config, select only input configs that contain sample/signal name
                if self.samples or self.signals:
                    for samplename in (self.samples + self.signals):
                        if samplename and samplename in config:
                            inputConfigs.append(os.path.join(inputConfig, config))
                            break
                # otherwise add all input configs
                else:
                    inputConfigs.append(os.path.join(inputConfig, config))

        # run configs for histogram creation with XAMPPplotting for selected region
        runConfigs = [ResolvePath(r) for r in self._regions[region]]

        # histogram configs for histogram creation with XAMPPplotting for selected region ("doFitInputs" limits histogram output to only the histograms needed for fit inputs)
        histoConfig = ResolvePath(self._histograms['Fitting_' + region if self.doFitInputs else region])

        # job name: "analysis" / custom name + SR/CR1/CR2ee/CR2mumu/PH + date and timestamp in format YYYY-MM-DD-hh-mm

        datestamp = datetime.now().strftime("%Y-%m-%d")
        jobname = datetime.now().strftime("%Y-%m-%d-%H-%M") + '_' + self._jobname + '_' + region

        # display information before submitting job
        logging.info("job name:" + jobname)
        logging.info("inputConfigs:\n" + pformat(inputConfigs))
        logging.info("runConfigs:\n" + pformat(runConfigs))
        logging.info("histoConfig:\n" + pformat(histoConfig))

        # cmd - command contains sublists (inputConfigs, runConfigs) and needs to be flattened by a solution involving itertools
        cmd = [
            'python',
            ResolvePath('XAMPPplotting/python/SubmitToBatch.py'), '--jobName',
            str(jobname), '--BaseFolder',
            str(self.outputBasePath), '--engine',
            str(self.engine), '--inputConf', inputConfigs, '--runConf', runConfigs, '--histoConf',
            str(histoConfig)
        ]
        cmd = list(itertools.chain.from_iterable(itertools.repeat(x, 1) if isinstance(x, str) else x for x in cmd))
        logging.info(pformat(cmd))
        subprocess.check_call(cmd)

        # plot creation
        histogramDir = os.path.join(self.outputBasePath, 'OUTPUT', datestamp, jobname)
        plotDir = os.path.join(self.outputBasePath, 'PLOTS', datestamp, jobname)
        if self.luminosity == "auto":
            DSConfig = self.createDSConfig(histogramDir, inputconfigs=inputConfigNames.values())
        else:
            DSConfig = self.createDSConfig(histogramDir, lumi=float(self.luminosity))
        CreateDirectory(plotDir)

        # check if data/mc or just mc plots should be made and check if signal will be included
        doDataMCPlots = False
        doSignalPlots = False
        for sample in os.listdir(histogramDir):
            if "data" in sample: doDataMCPlots = True
            for signal in self.signals:
                if signal in sample: doSignalPlots = False

        if doDataMCPlots:
            cmd = [
                'python',
                ResolvePath('XAMPPplotting/python/DataMCPlots.py'), '--outputDir',
                str(plotDir), '--config',
                str(DSConfig), '--noSignalStack' if doSignalPlots else '--noSignal'
            ]
        else:
            cmd = [
                'python',
                ResolvePath('XAMPPplotting/python/MCPlots.py'), '--outputDir',
                str(plotDir), '--config',
                str(DSConfig), '--noSignalStack' if doSignalPlots else '--noSignal'
            ]

        if not self.doSystematics:
            cmd.append('--noSyst')

        if self.LowerPadRanges:
            cmd.append('--LowerPadRanges')
            cmd.append(str(self.LowerPadRanges[0]))
            cmd.append(str(self.LowerPadRanges[1]))

        if self.variables:
            cmd.append('-v')
            for variable in self.variables:
                cmd.append(str(variable))

        cmd.append('-f')
        if not self.format: self.format = ['eps']
        for f in self.format:
            cmd1 = cmd[:]
            cmd1.append(str(f))
            logging.info(pformat(cmd1))
            subprocess.check_call(cmd1)

            # plots with logarithmic y-axis scale
            if self.doLogY:
                cmd1.append('--doLogY')
                logging.info(pformat(cmd1))
                subprocess.check_call(cmd1)

    def launch(self, jobname=''):
        """Launch execution."""
        if jobname: self._jobname = jobname
        # prepare mother run config file
        self.customiseRunConfigs()
        # process each region specified in config file
        for region in self.regions:
            self.processRegion(region)


def updateConfigFile(configFile):
    """Open config file with text editor and allow for modification before running plot creation."""
    subprocess.check_call(['nano', configFile])


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Produce histograms for mono-h(bb) analysis.")
    parser.add_argument('-n', '--name', default='', help='String to be added to the output directory to help with bookkeeping of plots.')
    parser.add_argument('-c',
                        '--config',
                        default=ResolvePath('XAMPPmonoH/runHistos_config.json'),
                        help='Path to configuration file for histogram/plot creation.')
    parser.add_argument('-e',
                        '--edit',
                        action='store_true',
                        help='Modify config file for this script prior to execution with a text editor.')
    return parser


def main():
    """Run local histogram production for Mono-h(bb) analysis."""
    RunOptions = getArgumentParser().parse_args()
    if RunOptions.edit:
        updateConfigFile(RunOptions.config)
    control = PlottingControl(RunOptions.config)
    control.launch(RunOptions.name)


if __name__ == "__main__":
    main()

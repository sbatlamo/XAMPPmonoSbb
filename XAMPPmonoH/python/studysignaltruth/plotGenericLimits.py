import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from argparse import ArgumentParser
from matplotlib.colors import LinearSegmentedColormap
from XAMPPmonoH.ATLASlabel import drawATLASLabel
from rootpy.plotting.style import set_style


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('inputFile', help='Csv file with significances for different signal points.')
    parser.add_argument('--mScalar', type=str, default=None, help='Scalar particle mass in GeV.')
    parser.add_argument('--mDM', type=str, default=None, help='Dark matter particle mass in GeV.')
    return parser.parse_args()


def findBetween(s, first, last=''):
    """Find substring between two substrings first and last in string s."""
    try:
        start = s.index(first) + len(first)
        if last:
            end = s.index(last, start)
            return s[start:end]
        else:
            return s[start:]
    except ValueError:
        prettyPrint("ERROR", "String `{s}` does not contain a substring between `{first}` and `{last}`".format(s=s, first=first, last=last))
        return ""


def getMZP(row):
    name = row['name']
    return findBetween(name, '_zp', '_')


def getMHS(row):
    name = row['name']
    return findBetween(name, '_dh', '')


def getMDM(row):
    name = row['name']
    return findBetween(name, '_dm', '_')


def main():
    """Plot significances from csv file."""
    args = getArgs()
    # read csv file with significances
    df = pd.read_csv(args.inputFile).dropna()
    variable = 'sensitivity'

    # augment dataframe with model parameters extracted from filename
    df['ScalarMass'] = df.apply(lambda row: getMHS(row), axis=1)
    df['MedMass'] = df.apply(lambda row: getMZP(row), axis=1)
    df['DarkMatterMass'] = df.apply(lambda row: getMDM(row), axis=1)

    # filter dark matter mass
    if args.mScalar:
        df = df.loc[df['ScalarMass'] == args.mScalar]
    elif args.mDM:
        df = df.loc[df['DarkMatterMass'] == args.mDM]

    muMin = float(df.loc[:, [variable]].min())
    muMax = float(df.loc[:, [variable]].max())
    delta = (muMax - muMin)
    if args.mScalar:
        df = df.pivot("DarkMatterMass", "MedMass", variable)
    elif args.mDM:
        df = df.pivot("ScalarMass", "MedMass", variable)

    # create color map
    cldict = {
        'red': ((0.0, 1.0, 1.0), (1. / delta, 221. / 256., 221. / 256.), (1.0, 0.0, 0.0)),
        'green': ((0.0, 1., 1.), (1. / delta, 222 / 256., 222 / 256.), (1.0, 119 / 256., 119 / 256.)),
        'blue': ((0.0, 1., 1.), (1. / delta, 214. / 256., 214. / 256.), (1.0, 117. / 256., 117. / 256.))
    }
    colourmap = LinearSegmentedColormap('mpp', cldict)

    # style
    set_style('ATLAS', mpl=True)

    # plot significances as heatmap
    f, ax = plt.subplots(figsize=(10, 8))
    sns.heatmap(df, ax=ax, annot=True, fmt='.1f', cmap=colourmap, cbar_kws={'label': 'Sensitivity'})

    plt.xlabel("Z' mass [GeV]", position=(1., 0.), va='bottom', ha='right')
    if args.mScalar:
        plt.ylabel('Dark matter mass [GeV]', position=(0., 1.), va='top', ha='right')
    elif args.mDM:
        plt.ylabel('Scalar boson  mass [GeV]', position=(0., 1.), va='top', ha='right')
    ax.xaxis.set_label_coords(1., -0.20)
    ax.yaxis.set_label_coords(-0.18, 1.)
    if args.mScalar:
        plt.title('Mono-s(bb): mS={hs} GeV, gq=0.25, gDM=1.0'.format(hs=args.mScalar))
    elif args.mDM:
        plt.title('Mono-s(bb): mDM={dm} GeV, gq=0.25, gDM=1.0'.format(dm=args.mDM))
    plt.gca().invert_yaxis()
    drawATLASLabel(0.65, 0.15, ax, 'int', energy='13 TeV', lumi=36.1)
    if args.mScalar:
        plt.savefig('sensitivity_massScalar{hs}.eps'.format(hs=args.mScalar))
    elif args.mDM:
        plt.savefig('sensitivity_massDM{dm}.eps'.format(dm=args.mDM))


if __name__ == '__main__':
    main()

#!/bin/env python

import os
import logging
from argparse import ArgumentParser

# output verbosity
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("inputDirectory", help="Directory with ROOT files to be scanned.")
    parser.add_argument("-o",
                        "--outputDirectory",
                        default="XAMPPmonoH/data/GroupDiskLists/MPPMU_LOCALGROUPDISK",
                        help="Output directory for sample lists.")
    return parser


def scanDir(directory):
    """Scan directory for root files and return them as dictionary.
    The directory structure should be that for each sample there is
    a subfolder in which the ROOT files are stored.
    Example: base/999000.MadGraphPy8EG_A14NNP23LO/MC15.999000.seed1.root
    """
    result = {}
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.root'):
                filepath = os.path.join(root, file)
                logging.info(filepath)
                sample = os.path.basename(root)
                if sample not in result:
                    result.update({sample: []})
                result[sample].append(filepath)
    return result


def writeSampleLists(files, outputDirectory):
    """Write sample lists from dictionary to output directory."""
    if not os.path.exists(outputDirectory):
        os.makedirs(outputDirectory)
    for sample, data in files.items():
        with open(os.path.join(outputDirectory, sample + '.txt'), 'w') as f:
            f.write('\n'.join(data))


def createSampleList():
    """Create sample list from files in directory."""
    args = getArgs().parse_args()
    logging.info("Reading files in {d}".format(d=args.inputDirectory))
    logging.info("Storing output in  {d}".format(d=args.outputDirectory))

    files = scanDir(args.inputDirectory)

    writeSampleLists(files, args.outputDirectory)


if __name__ == '__main__':
    createSampleList()

#!/bin/usr/env python
"""
This script prints out the cross sections of simulated processes from log files of the event generation.
Author: paul.philipp.gadow@cern.ch
"""
from argparse import ArgumentParser
from collections import OrderedDict
import json


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("logFile", nargs='+', help="Path to log file from event generation, contains cross-section information.")
    parser.add_argument("-o", "--outFile", default=None, help="Write to output file (only if specified)")
    parser.add_argument(
        "-g",
        "--generator",
        default=None,
        help="Specify which MC generator was used for event generation. If not specified, try to determine from log file structure.")
    return parser


def prettyPrint(preamble, data, width=30, separator=":"):
    """Prints uniformly-formatted lines of the type "preamble : data"."""
    preamble = preamble.ljust(width)
    print "{pre}{sep} {data}".format(pre=preamble, sep=separator, data=data)


def getGeneratorFromLogFile(logFile):
    """Scan log file to determine generator."""
    model = "None"
    with open(logFile) as f:
        for line in f.readlines():
            # search for madgraph/amcatnlo
            if "MadGraph5_aMC@NLO" in line:
                model = "mg5amcatnlo"
            # specify: check if amcatnlo
            if model == "mg5amcatnlo" and "run_card.dat aMC@NLO" in line:
                model = "amcatnlo"
                break
            # specify: check if madgraph
            elif model == "mg5amcatnlo" and "run_card.dat MadEvent" in line:
                model = "madgraph"
                break
    return model


def findBetween(s, first, last):
    """Find substring between two substrings first and last in string s."""
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        prettyPrint("ERROR", "String `{s}` does not contain a substring between `{first}` and `{last}`".format(s=s, first=first, last=last))
        return ""


def getCrossSectionAndUncertainty(line, keyword):
    """Get cross section and uncertainty as list from line split by keyword."""
    result = str(line).split(keyword)[1].split('+-')
    crossSection = float(result[0])
    uncertainty = float(result[1].replace('pb\n', ''))
    return [crossSection, uncertainty]


def getSystematic(line, keyword):
    """Get systematic variation uncertainties as list from line split by keyword."""
    result = str(line).split(keyword)[1].split('% ')
    sys_up = float(result[0].replace('+', '').strip()) * 0.01
    sys_do = result[1].replace('%\n', '').strip()
    sys_do = (-float(sys_do.replace('-', '')) if '-' in sys_do else float(sys_do)) * 0.01
    return [sys_up, sys_do]


def getValue(line, keyword):
    """Get value(e.g. branching ratio, number of events) without uncertainty from line split by keyword."""
    value = str(line).split(keyword)[1].replace("\n", "")
    return value


def extractData(filename, keywords):
    """General class for extracting information from event generation log files."""
    data = OrderedDict()

    with open(filename, 'r') as f:
        for line in f.readlines():
            for key, item in keywords.items():
                # cross-section
                if item in line and key == 'xsec':
                    data[key] = getCrossSectionAndUncertainty(line, item)

                # branching ratio
                if item in line and key == 'branchingratio':
                    data[key] = float(getValue(line, item))

                # number of generated events
                if item in line and key == 'nevents':
                    data[key] = int(getValue(line, item))

                # filter efficiency
                if item in line and key == 'filter':
                    data[key] = getValue(line, item)

                # meta data total cross-section in (converted from nb to pb)
                if item in line and key == 'totalxsec':
                    data[key] = float(getValue(line, item)) * 1000.

                # scale uncertainty
                if item in line and key == 'sys_scale':
                    data[key] = getSystematic(line, item)

                # central scheme uncertainty
                if item in line and key == 'sys_centralscheme':
                    data[key] = getSystematic(line, item)

                # central scheme uncertainty
                if item in line and key == 'sys_pdf':
                    data[key] = getSystematic(line, item)

    return data


def getInfoFromMG5(log):
    """Get cross-section from MadGraph 5 log file.
    Typical format: Cross-section :   118.7 +- 0.1681 pb
                    Nb of events :  30000
    """
    keywords = {
        'xsec': 'Cross-section :',
        'nevents': 'CountHepMC           INFO Events passing all checks and written = ',
        'filter': 'MetaData: GenFiltEff = ',
        'totalxsec': 'MetaData: cross-section (nb)= ',
        'sys_scale': 'scale variation:',
        'sys_centralscheme': 'central scheme variation:',
        'sys_pdf': 'PDF variation:'
    }
    result = extractData(log, keywords)

    # append branching fraction information
    result['branchingratio'] = 1.

    return result


def getInfoFromaMCAtNLO(log):
    """Get cross-section from getXSecFromaMCAtNLO log file.
    Typical format: ?
    """
    keywords = {
        'xsec': 'Total cross section:',
        'branchingratio': 'Branching ratio to allowed decays:',
        'nevents': 'CountHepMC           INFO Events passing all checks and written = ',
        'filter': 'MetaData: GenFiltEff = ',
        'totalxsec': 'MetaData: cross-section (nb)= '
    }
    result = extractData(log, keywords)
    return result


def getSampleName(logFile):
    """Get DSID from log file."""
    samplename = ""
    with open(logFile) as f:
        for line in f:
            if "MC15." in line:
                samplename = findBetween(line, 'MC15.', '.py').split('.')[1]
                break

    return samplename


def getDSID(logFile):
    """Get DSID from log file."""
    dsid = 999999
    with open(logFile) as f:
        for line in f:
            if "MC15." in line:
                dsid = int(findBetween(line, 'MC15.', '.py').split('.')[0])
                break
    return dsid


def getSeed(logFile):
    """Get seed from log file."""
    seed = 0
    with open(logFile) as f:
        for line in f:
            if "= iseed   ! rnd seed" in line:
                seed = int(line.split('=')[0].split()[1].strip())
                break

    return seed


def main():
    # get paths to log files from command line input
    args = getArgs().parse_args()
    logFiles = args.logFile
    generator = args.generator

    data = {}

    for logFile in logFiles:
        # determine generator from log file
        if not generator:
            gen = getGeneratorFromLogFile(logFile)
        else:
            gen = generator

        # find DSID and seed from log file
        dsid = getDSID(logFile)
        seed = getSeed(logFile)

        # additional layer: seeds
        if dsid not in data.keys():
            data[dsid] = {}

        # check info for different generators
        info = OrderedDict()
        if gen == 'madgraph':
            info = getInfoFromMG5(logFile)
        elif gen == 'amcatnlo':
            info = getInfoFromaMCAtNLO(logFile)

        # append generator information
        info['generator'] = gen

        # try to append xsec \times branchingratio
        try:
            info['xsecXbr'] = float(info['xsec'][0]) * float(info['branchingratio'])
        except Exception as e:
            prettyPrint("ERROR", "Could not calculate cross section times branching ratio for {dsid}_{seed}".format(dsid=dsid, seed=seed))
            print(e)

        # append sample name
        info['samplename'] = getSampleName(logFile)

        # add to output
        try:
            assert "totalxsec" in info.keys()
            data[dsid][seed] = info
        except AssertionError as e:
            prettyPrint(
                "ERROR",
                "Log file {log} for DSID {sample} with seed {seed} does not contain cross-section. Skipping sample...".format(log=logFile,
                                                                                                                              sample=dsid,
                                                                                                                              seed=seed))
            print(e)

    # print output
    print(json.dumps(data, indent=4, sort_keys=True))

    # write output to file
    if args.outFile:
        with open(args.outFile, 'w') as outFile:
            outFile.write(json.dumps(data, indent=4, sort_keys=True))


if __name__ == "__main__":
    main()

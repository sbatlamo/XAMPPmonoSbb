######################
#Author : Sam Meehan <samuel.meehan@cern.ch>
#
#This is a collection of small helper functions
#
######################

from ROOT import *


def GetHist1D(file, clonename, dirname, histname, default_name, debugMode):
    h = None
    try:
        if debugMode == "True":
            print "GetHist1D", dirname, "  -  ", histname

        if dirname == "":
            if bool(file.GetListOfKeys().Contains(histname)):
                if debugMode == "True":
                    print "FoundNom:", histname
                h = file.Get(histname).Clone(clonename)
            else:
                if debugMode == "True":
                    print "DummyNom: ", default_name
                h = file.Get(default_name).Clone(clonename)
                h.Reset()

        else:
            if bool(file.GetListOfKeys().Contains(dirname)):
                if debugMode == "True":
                    print "FoundDirSyst:", dirname
                d = file.Get(dirname)
                if bool(d.GetListOfKeys().Contains(histname)):
                    if debugMode == "True":
                        print "FoundHistSyst:", histname
                    h = d.Get(histname)
                else:
                    if debugMode == "True":
                        print "DummySyst: ", clonename
                    h = file.Get(default_name).Clone(clonename)
                    h.Reset()

            else:
                if debugMode == "True":
                    print "Dummy: ", clonename
                h = file.Get(default_name).Clone(clonename)
                h.Reset()

        h.SetDirectory(0)

        #c = TCanvas("c","c",400,400)
        #h.Draw("hist")
        #c.SaveAs("testGetHist1D_"+clonename+".pdf")

    except Exception:
        print('Oooops, could not get histogram. Could you check a bit if it is present in the input file for {histname}'.format(
            histname=histname))

    return h


def MakeHistSum(name, hist_array):
    h = None
    try:
        h = hist_array[0].Clone(name)
        h.SetDirectory(0)

        for i in range(1, len(hist_array)):
            if hist_array[i] is not None:
                h.Add(hist_array[i])
    except Exception:
        h = hist_array[1].Clone(name)
        h.SetDirectory(0)

        for i in range(2, len(hist_array)):
            if hist_array[i] is not None:
                h.Add(hist_array[i])

    return h

#! /usr/bin/env python
import os
import re
from argparse import ArgumentParser
from collections import Counter
from ClusterSubmission.PeriodRunConverter import getGRL
from ClusterSubmission.Utils import ResolvePath
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None

if TestArea is None:
    TestArea = '.'


def getArgParser():
    """Get arguments from command line."""
    parser = ArgumentParser(
        description=
        'This script checks if you have duplicate DSIDs in your sample list (i.e. list with DxAOD names that are used for ntuple production). \
        The file names of the sample list must follow this format: "data[15,16,17]_[derivationname].txt" or "mc16[a,d,e]_[signals,bkgs]_[derivationname].txt" \
        The script checks the sample list for completeness based on the GRL in case for data or the content of XAMPPmonoH/data/samplelist.txt'
    )
    parser.add_argument('inputFile', help='your input file with data/MC lists')
    parser.add_argument('--GRL', help='your input GRL', default='')
    parser.add_argument('--derivation', help='your input derivation (choose HIGG or EXOT), leave blank for automatic detection', default='')
    return parser


def checkForDuplicates(infile):
    logging.info('<checkForDuplicates> Checking input file {inputfile} for duplicated runs/DSIDs!'.format(inputfile=infile))
    fileToOpen = open(str(infile), "r")
    nfiles = 0
    checkList = []
    for line in fileToOpen:
        line = line.strip()
        logging.debug("Processing line {line}".format(line=line))

        # ignore comments
        if line.startswith('#') or not line:
            continue
        checkList.append(line.split('.')[1])
        nfiles += 1

    checkedList = [k for k, v in Counter(checkList).items() if v > 1]
    if len(checkedList) > 0:
        logging.info('There are {nfiles} files in your sample list, and {nduplicates} duplicates have been found!'.format(
            nfiles=nfiles, nduplicates=len(checkedList)))
        logging.info(
            'Note that there might be extensions of datasets, indicated by multiple etags. In this case it is ok to have duplicate DSIDs.')
        for dup in checkedList:
            logging.warning('Please check the following runs/DSIDs: {dup}'.format(dup=dup))
            for i in fileToOpen:
                logging.warning(i)
                if dup in i:
                    logging.warning('Please check the following files:')
                    logging.warning(i)
    else:
        logging.info('There are {nfiles} files in your sample list, and no duplicates have been found!'.format(nfiles=nfiles))
    return checkList


def numberOfRunsInGRL(grlFile):
    if '.xml' not in grlFile:
        logging.error("Please provide the right GRL (ending with .xml)")
        exit(1)
    grlFileToOpen = open(str(grlFile), "r")
    numberOfRuns = []
    for line in grlFileToOpen:
        line = line.strip()
        if 'RunList' in line:
            numberOfRuns = line.split('>')[1].split('<')[0].split(',')
    if len(numberOfRuns) > 0:
        logging.info("The number of runs on the GRL {grl} is {nruns}.".format(grl=str(grlFile), nruns=str(len(numberOfRuns))))
    return numberOfRuns


def getProcessInfo(dsid, samplelist):
    """Search samplelist for process DSID and return string containing the corresponding line of the samplelist file with additional information about the process."""
    result = ""
    with open(samplelist) as origin_file:
        for line in origin_file:
            found_line = re.findall(str(dsid), line)
            if found_line:
                result = line.strip()
    return result


def main():
    """Check sample lists for duplicates and for consistency with good run list."""
    RunOptions = getArgParser().parse_args()
    listOfSamples = checkForDuplicates(RunOptions.inputFile)

    # ---------------------------------------------------------------------------------
    # DATA: check good run list
    # ---------------------------------------------------------------------------------
    GRL = []
    if 'data' in os.path.basename(RunOptions.inputFile):
        if len(RunOptions.GRL) > 0:
            GRL = RunOptions.GRL
        else:
            # default location of GRL
            GRL_paths = {
                'data15': ResolvePath(str(getGRL(15)[0])),
                'data16': ResolvePath(str(getGRL(16)[0])),
                'data17': ResolvePath(str(getGRL(17)[0])),
                'data18': ResolvePath(str(getGRL(18)[0]))
            }
            if 'data15' in RunOptions.inputFile:
                GRL = GRL_paths['data15']
            if 'data16' in RunOptions.inputFile:
                GRL = GRL_paths['data16']
            if 'data17' in RunOptions.inputFile:
                GRL = GRL_paths['data17']
            if 'data18' in RunOptions.inputFile:
                GRL = GRL_paths['data18']

        # check good run list
        if len(GRL) > 0:
            grlList = numberOfRunsInGRL(GRL)

            for i in listOfSamples:
                i = i[2:]
                if i not in grlList:
                    logging.warning("You have included the run {run} but this run is not part of the GRL!".format(run=i))

            missingOnGRL = False
            for i in grlList:
                i = "00" + str(i)
                if i not in listOfSamples:
                    missingOnGRL = True
                    logging.warning("You are missing run {run}".format(run=i))
            if missingOnGRL is False:
                logging.info("You are not missing any run of the GRL, that is good!")

    # ---------------------------------------------------------------------------------
    # MC: check if all DSIDs are present against reference file
    # ---------------------------------------------------------------------------------
    if 'mc' in os.path.basename(RunOptions.inputFile) and 'data' not in os.path.basename(RunOptions.inputFile):
        # check which derivation is used, if not specified try autodetect
        derivation = RunOptions.derivation
        if not derivation:
            derivation = RunOptions.inputFile.split('_')[-1].replace('.txt', '')

        # read text file with all dsid of required samples
        datadir = os.path.join(TestArea, 'XAMPPmonoH', 'data')
        if not os.path.isdir(datadir):
            logging.error('Cannot locate XAMPPmonoH/data')
            exit(1)

        infofile = os.path.join(datadir, 'samplelist.txt')
        if not os.path.isfile(infofile):
            logging.error('{} is not a valid file'.format(infofile))
            exit(1)
        dsids = []
        vetos = []
        with open(infofile) as f:
            for line in f.readlines():
                try:
                    dsid = int(line.split()[0])
                except Exception:
                    # ignore empty or commented lines
                    continue
                checkDerivation = False
                if 'EXOT' in line or 'HIGG' in line:
                    checkDerivation = True
                # add dsid to list if it matches derivation or no derivation has been specified
                if checkDerivation:
                    if derivation in line:
                        dsids.append(str(dsid))
                else:
                    dsids.append(str(dsid))

                # check for vetos
                if "veto" in line:
                    vetos.append(str(dsid))

                # veto signals for background sample list and vice versa
                if 'signals' in os.path.basename(RunOptions.inputFile) and "zp2hdmbb" not in line:
                    vetos.append(str(dsid))

                if 'bkgs' in os.path.basename(RunOptions.inputFile) and "zp2hdmbb" in line:
                    vetos.append(str(dsid))

        if len(dsids) > 0:
            missingSamples = False
            for i in dsids:
                if i in vetos:
                    continue
                if i not in listOfSamples:
                    missingSamples = True
                    logging.warning("You are missing dsid {dsid}".format(dsid=getProcessInfo(i, infofile)))
                    logging.warning("Check using: rucio ls mc16_13TeV.{dsid}.*.DAOD_{derivation}.* --short --filter type=CONTAINER".format(
                        dsid=i, derivation=derivation))
            if missingSamples is False:
                logging.info("You are not missing any MC samples specified on {reference}, thats good!".format(reference=infofile))


if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""
Purpose: This script creates sample lists for the mono-h(bb) analysis including the latest derivation cache tags for data and MC (signal + backgrounds).
Usage: Enter `python CreateSampleList.py -h` for some advice.

"""

import os
import sys
import logging
import json
import re
import commands
import xml.etree.ElementTree as ET
from argparse import ArgumentParser
from ClusterSubmission.AMIDataBase import getAMIDataBase
from ClusterSubmission.PeriodRunConverter import getGRL
from ClusterSubmission.Utils import CheckRemainingProxyTime, CheckAMISetup, CheckRucioSetup, ResolvePath, ClearFromDuplicates, WriteList

# verbosity
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def getArguments():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("-c", "--config", help="Path to configuration file", default=ResolvePath("XAMPPmonoH/samplelist_config.json"))
    parser.add_argument("--appyVetoForAOD",
                        action="store_true",
                        help="Skip samples with veto flag on the samplelist.txt also for AOD lists.")
    parser.add_argument("--skipCheckGRL", action="store_true", help="Skip check if the GRLs in the config file are up to date.")
    parser.add_argument(
        "--noDataLists",
        action="store_true",
        help=
        "Don't update data sample lists, only update MC data lists (e.g. if you have defined period containers and don't want those overwritten)."
    )
    parser.add_argument(
        '--pTags',
        nargs='+',
        help=
        'Option to choose specific p-tags (type e.g. pTags p3840 p3841 to get the MC samples with p3840 and the data samples with p3841). This is useful when more than one derivation versions are available.'
    )
    return parser


class Sample(object):
    """
    Sample / certain dataset
    """
    def __init__(self, dsid, name="", veto=False, derivation=[]):
        super(Sample, self).__init__()
        # dsid (dataset identifier) of sample, 6 digit number, e.g. 310354
        self.dsid = dsid
        # human readable name of sample
        self.name = name
        # veto sample? (True / False)
        self.veto = veto
        # produce sample only for certain derivation(s)
        self.derivation = derivation

    def __repr__(self):
        return "dsid: {dsid}\tname: {name}\tveto: {veto}\tderivations: [{derivations}]".format(dsid=self.dsid,
                                                                                               name=self.name,
                                                                                               veto="True" if self.veto else "False",
                                                                                               derivations=" ".join(self.derivation))

    @staticmethod
    def makeSample(line, derivations=[]):
        """
        Factory method to create a sample out of a line in the samplelist.txt
        file. The samplelist.txt file is composed of entries that are of this
        form: 345110 qqWlvHccJ_PwPy8MINLO [veto] [EXOT27]   # the "veto" and
        names of derivations are optional This method is supposed to be called
        for each line of the samplelist, which is then split into dsid (1st
        entry) and name of sample (2nd entry).

        The derivations argument of this method should list all derivations
        which could be relevant for the samples in the list.

        @param      line         A line from the samplelist.txt file in the
                                 format 345110 qqWlvHccJ_PwPy8MINLO [veto]
                                 [EXOT27]   # the "veto" and names of
                                 derivations are optional
        @param      derivations  The derivations that potentially could be
                                 defined for the analysis and the samples on the
                                 list

        @return     a Sample object
        """
        try:
            info = line.split()
            dsid = info[0]
            name = info[1]
            derivation = []
            useDefaultDerivations = True
            if derivations:
                for d in derivations:
                    if d in info:
                        derivation.append(d)
                        useDefaultDerivations = False
                if useDefaultDerivations: derivation.extend(derivations)

            veto = 'veto' in info
            return Sample(dsid, name, veto, derivation)
        except Exception as e:
            logging.warning("Could not create sample out of line:")
            logging.warning(line)
            logging.warning("Exception: " + str(e))
            return None

    def getDataSet(self, name, tag, campaign='mc16_13TeV', dataset='AOD', appyVetoForAOD=False, derivations=[]):
        # AOD dataset name
        if dataset == 'AOD':
            if self.veto and appyVetoForAOD: return None
            return "{campaign}.{dsid}.{name}.recon.AOD.{tag}".format(campaign=campaign, dsid=self.dsid, name=name, tag=tag)
        elif dataset in derivations:
            if self.veto: return None
            return "{campaign}.{dsid}.{name}.deriv.DAOD_{dataset}.{tag}".format(campaign=campaign,
                                                                                dsid=self.dsid,
                                                                                name=name,
                                                                                dataset=dataset,
                                                                                tag=tag)
        return None


class SampleContainer(object):
    """Container to hold samples"""
    def __init__(self):
        super(SampleContainer, self).__init__()
        # dict of samples: identifier is dsid, value is sample object
        self._samples = {}
        self._derivations = set()

    def __repr__(self):
        return "\n".format(self._samples.values())

    def __getitem__(self, key):
        try:
            return self._samples[key]
        except Exception:
            return None

    def derivations(self):
        return list(self._derivations)

    def keys(self):
        return self._samples.keys()

    def values(self):
        return self._samples.values()

    def items(self):
        return self._samples.items()

    def add(self, sample):
        """Add sample to container."""
        try:
            assert isinstance(sample, Sample)
            self._samples[sample.dsid] = sample
            self._derivations.update(sample.derivation)
        except AssertionError as e:
            logging.warning("Object could not be added to sample container, not a sample!")
            logging.warning(e)


class SampleListCreator(object):
    """Creates sample lists based on configuration files."""
    def __init__(self, config):
        super(SampleListCreator, self).__init__()
        self.samplelist = ""
        self.outputDir = ""
        self.campaign = ""
        self.rtags = {}
        self.derivations = []
        self.blacklist = []
        self.signals = []

        self.GRL = {}

        self.doFullSim = True
        self.doFastSim = True

        self._samples = SampleContainer()
        self.readConfig(config)

        # process inputs
        self.processInput()

        # create lists of final MC datasets for campaigns and for AOD / requested DxAODs
        for aod in ['AOD'] + self._samples.derivations():
            for campaign in self.rtags.keys():
                try:
                    setattr(self, '_' + aod + '_' + campaign, [])
                except Exception as e:
                    logging.warning('Could not create list for datasets for {a} {c}'.format(a=aod, c=campaign))
                    logging.warning(e)

            # create lists of final data datasets for years
            for year in self.GRL.keys():
                try:
                    setattr(self, '_' + aod + '_' + year, [])
                except Exception as e:
                    logging.warning('Could not create list for datasets for {c}'.format(c=year))
                    logging.warning(e)

    def __repr__(self):
        """Return JSON-style formatted string for all class members that are no functions and do not start with '_'."""
        return str(
            json.dumps(
                {attr: value
                 for attr, value in self.__dict__.items() if not callable(getattr(self, attr)) and not attr.startswith("_")},
                indent=4))

    def addSample(self, sample):
        """Add sample to sample container."""
        self._samples.add(sample)

    def readConfig(self, configPath):
        """Read configuration file formated in json to extract information to fill TemplateMaker variables."""
        try:
            stream = open(configPath, 'r')
            config = json.loads(stream.read())
            member_variables = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("_")]
            # check if member variable name is in config file. if it is, update member variable to value of config file
            for member in config.keys():
                if member in member_variables:
                    setattr(self, member, config[member])
        except Exception as e:
            logging.error("Could not read configuration '{config}'".format(config=configPath))
            logging.error("Exception: " + str(e))

    def checkGRL(self, skipCheckGRL):
        """Check if the GRLs on the config file are in sync with XAMPPbase."""
        if skipCheckGRL: return
        for year, GRL in self.GRL.items():
            if GRL not in getGRL():
                logging.warning("GRL might be outdated! Check GRL for {year}".format(year=year))
                logging.warning("Used GRL: {grl}".format(grl=GRL))
                logging.warning("Recommended GRLs in ClusterSubmission/data/GRL.json:")
                logging.warning("\n".join(getGRL()))
                logging.warning("Exiting now. If you want to skip this check, use the option --skipCheckGRL")
                sys.exit(1)

    def processInput(self):
        """Read samplelist.txt file with DSIDs for which samples should be processed."""
        samplelist = ResolvePath(self.samplelist)
        logging.info("Reading {s} and storing samples internally...".format(s=samplelist))
        with open(samplelist) as f:
            for line in f:
                # ignore comments or empty lines
                if line.strip().startswith('#') or not line.strip(): continue
                sample = Sample.makeSample(line, self.derivations)
                self.addSample(sample)
                logging.info(sample)

    def passTagQuality(self, tag):
        # only use tags that contain "_e" and have only one "_r", "_s" and "_a" tag
        if tag.find("_e") != -1 or len([x for x in ["r", "s", "a"] if tag.rfind("_%s" % (x)) != tag.find("_{s}".format(s=x))]) > 0:
            return False
        # filter for fast sim ("_a") or full sim ("_s") samples
        if tag.find("_a") != -1 and not self.doFastSim: return False
        if tag.find("_s") != -1 and not self.doFullSim: return False
        # if tag passes criteria, return True
        return True

    def queryAMI(self, appyVetoForAOD=False):
        """Query AMI for all samples."""
        ###############
        # MC samples  #
        ###############
        logging.info("Query AMI about blocks of datasets starting with first 3 digits of dsid.")
        logging.info("Some warnings are expected because also datasets you are not interested in and which may be incomplete are queried.")

        getAMIDataBase().getMCDataSets(channels=self._samples.keys(),
                                       campaign=self.campaign,
                                       derivations=['DAOD_' + d for d in self._samples.derivations()])
        if appyVetoForAOD: logging.info("Skipping samples with veto flags.")

        # loop over MC samples and query ami for each sample
        for sample in self._samples.values():
            try:
                info = getAMIDataBase().getMCchannel(int(sample.dsid), campaign=self.campaign)
                if not info:
                    logging.error("Not in AMI: sample {sample} for campaign {campaign}!".format(sample=str(sample), campaign=self.campaign))
                    continue
                # search for AOD datasets
                for tag in sorted(info.getTags(data_type="AOD")):
                    if not self.passTagQuality(tag): continue
                    # check if the associated AOD exists and if it has events associated
                    dataset = sample.getDataSet(info.name(), tag, campaign=self.campaign, dataset='AOD', appyVetoForAOD=appyVetoForAOD)
                    events = info.getEvents(tag, 'AOD')
                    if not dataset:
                        logging.warning("{aod} dataset for dsid {dsid} and tag {tag} not found!".format(aod='AOD',
                                                                                                        dsid=sample.dsid,
                                                                                                        tag=tag))
                        continue
                    if not events:
                        logging.warning("{aod} dataset {ds} for dsid {dsid} and tag {tag} contains no events!".format(aod='AOD',
                                                                                                                      ds=ds,
                                                                                                                      dsid=sample.dsid,
                                                                                                                      tag=tag))
                        continue
                    # sort samples to internal MC campaign lists
                    for campaign, rtag in self.rtags.items():
                        try:
                            if rtag in tag: getattr(self, '_' + 'AOD' + '_' + campaign).append(dataset)
                        except Exception as e:
                            logging.warning('Could not add {aod} dataset {ds} to internal list of datasets'.format(aod='AOD', ds=dataset))
                            logging.warning(e)
                # search for DAOD datasets for all derivations that were specified
                for daod in self._samples.derivations():
                    for tag in sorted(info.getTags(data_type='DAOD_' + daod)):
                        if not self.passTagQuality(tag): continue
                        # check if the associated DxAOD exists and if it has events associated
                        dataset = sample.getDataSet(info.name(),
                                                    tag,
                                                    campaign=self.campaign,
                                                    dataset=daod,
                                                    appyVetoForAOD=appyVetoForAOD,
                                                    derivations=self._samples.derivations())
                        events = info.getEvents(tag, 'DAOD_' + daod)
                        if not dataset:
                            logging.warning("{aod} dataset for dsid {dsid} and tag {tag} not found!".format(aod=daod,
                                                                                                            dsid=sample.dsid,
                                                                                                            tag=tag))
                            continue
                        if not events:
                            logging.warning("{aod} dataset {ds} for dsid {dsid} and tag {tag} contains no events!".format(aod=daod,
                                                                                                                          ds=ds,
                                                                                                                          dsid=sample.dsid,
                                                                                                                          tag=tag))
                            continue
                        # sort samples to internal MC campaign lists
                        for campaign, rtag in self.rtags.items():
                            try:
                                if rtag in tag: getattr(self, '_' + daod + '_' + campaign).append(dataset)
                            except Exception as e:
                                logging.warning('Could not add {aod} dataset {ds} to internal list of datasets'.format(aod=daod,
                                                                                                                       ds=dataset))
                                logging.warning(e)
            except Exception as e:
                logging.error("Problem occured when querying AMI and processing information about sample {s} ({dsid})".format(
                    s=sample.name, dsid=sample.dsid))
                logging.error(e)

        ###############
        # Data runs   #
        ###############
        logging.info("Query AMI about data runs.")
        # loop over years and query AMI for each sample
        project = self.campaign.split('_')[1]  # something like '13TeV'
        for year, GRLpath in sorted(self.GRL.items()):
            getAMIDataBase().loadRuns(int(year.replace('data', '')),
                                      derivations=['DAOD_' + d for d in self._samples.derivations()],
                                      project=project)
            # get GRL and parse it
            GRLpath = ResolvePath(str(GRLpath))
            tree = ET.parse(GRLpath)
            root = tree.getroot()
            # this is a bit fragile... when the GRL changes, it could break here :) - have fun, future me!
            runsOnGRL = root[0][5 if year == 'data16' else 4].text.split(',')
            # loop over runs of GRLs and sort samples to internal data lists
            # use AODs with highest r-tag preferably, if this does not exist, use f-tag (data directly from Tier-0)
            for run in runsOnGRL:
                logging.info("Processing data: {year} run {run} on GRL".format(year=year, run=run))
                info = getAMIDataBase().getRunElement(int(run))

                # AOD
                found_rtag = 'r0000_p0000'
                found_ftag = 'f000_m0000'
                for tag in sorted(info.tags()):
                    match_rtag = re.match("(r)([0-9]+)(_p)([0-9]+)", tag)
                    match_ftag = re.match("(f)([0-9]+)(_m)([0-9]+)", tag)
                    if match_rtag and int(match_rtag.group(2)) >= int(found_rtag.split('_')[0].replace('r', '')): found_rtag = tag
                    if match_ftag and int(match_ftag.group(2)) >= int(found_ftag.split('_')[0].replace('f', '')): found_ftag = tag
                datatag = found_rtag if found_rtag != 'r0000_p0000' else found_ftag
                dataset = year + '_' + project + '.' + '00' + str(
                    info.runNumber()) + '.' + 'physics_Main' + '.' + 'merge' + '.' + 'AOD' + '.' + datatag
                try:
                    getattr(self, '_' + 'AOD' + '_' + year).append(dataset)
                except Exception as e:
                    logging.warning('Could not add dataset {ds} to internal list of datasets'.format(ds=dataset))
                    logging.warning(e)

                # DxAODs
                for daod in self._samples.derivations():
                    found_rtag = 'r0000_p0000'
                    found_ftag = 'f000_m0000'
                    for tag in sorted(info.tags(data_type='DAOD_' + daod)):
                        match_rtag = re.match("(r)([0-9]+)(_p)([0-9]+)", tag)
                        match_ftag = re.match("(f)([0-9]+)(_m)([0-9]+)", tag)
                        if match_rtag and int(match_rtag.group(2)) >= int(found_rtag.split('_')[0].replace('r', '')): found_rtag = tag
                        if match_ftag and int(match_ftag.group(2)) >= int(found_ftag.split('_')[0].replace('f', '')): found_ftag = tag
                    datatag = found_rtag if found_rtag != 'r0000_p0000' else found_ftag
                    dataset = year + '_' + project + '.' + '00' + str(
                        info.runNumber()) + '.' + 'physics_Main' + '.' + 'deriv' + '.' + 'DAOD_' + daod + '.' + datatag
                    try:
                        getattr(self, '_' + daod + '_' + year).append(dataset)
                    except Exception as e:
                        logging.warning('Could not add dataset {ds} to internal list of datasets'.format(ds=dataset))
                        logging.warning(e)

    def checkDataSets(self):
        """Check MC datasets for completeness."""
        reference_dsids = set(self._samples.keys())
        for aod in (['AOD'] + self._samples.derivations()):
            for campaign, rtag in self.rtags.items():
                try:
                    # anatomy of dataset name: <campaign>.<dsid>.<physicsname>.recon/deriv.AOD/DAOD_<derivation>.<tag>
                    found_dsids = set([dataset.split('.')[1] for dataset in ClearFromDuplicates(getattr(self, '_' + aod + '_' + campaign))])
                    missing_dsids = reference_dsids.difference(found_dsids)
                    missing_samples = sorted([str(self._samples[dsid]) for dsid in list(missing_dsids)])
                    # write output file lists with missing samples
                    filename = campaign + '_' + 'missing' + '_' + aod + '.txt'
                    outputpath = os.path.join(self.outputDir, filename)
                    if missing_samples: WriteList(missing_samples, outputpath)
                    logging.info("Wrote list of missing samples to {o}".format(o=outputpath))

                except Exception as e:
                    logging.warning('Could not compare datasets for completeness for {c}'.format(c=campaign))
                    logging.warning(e)

    def isSignal(self, dataset):
        """Check if a dataset is a signal dataset."""
        isSignal = False
        dsname = dataset.replace('_', '')
        for signal in self.signals:
            if signal in dsname: isSignal = True
        return isSignal

    def checkPTags(self, datasets_list, ptags):
        """Check if derivation has the required ptag and remove all other derivation files from sample list."""
        datasets_list_ptag = []
        for ds in datasets_list:
            found_ptag = 0
            for p in ptags:
                if ds.endswith(p):
                    found_ptag += 1
            if (found_ptag == 1):
                datasets_list_ptag.append(ds)
        return datasets_list_ptag

    def writeSampleLists(self, nodatalists, ptags):
        """Write a list for each MC campaign"""
        for aod in (['AOD'] + self._samples.derivations()):
            for campaign in sorted(self.rtags.keys()):
                try:
                    datasets_bkgs = sorted(
                        [d for d in ClearFromDuplicates(getattr(self, '_' + aod + '_' + campaign)) if not self.isSignal(d)])
                    filename_bkgs = campaign + '_' + 'bkgs' + '_' + aod + '.txt'
                    outputpath_bkgs = os.path.join(self.outputDir, filename_bkgs)

                    datasets_signals = sorted(
                        [d for d in ClearFromDuplicates(getattr(self, '_' + aod + '_' + campaign)) if self.isSignal(d)])
                    filename_signals = campaign + '_' + 'signals' + '_' + aod + '.txt'
                    outputpath_signals = os.path.join(self.outputDir, filename_signals)

                    if ptags:
                        if aod in self._samples.derivations():
                            datasets_bkgs_ptag = self.checkPTags(datasets_bkgs, ptags)
                            datasets_signals_ptag = self.checkPTags(datasets_signals, ptags)
                            WriteList(datasets_bkgs_ptag, outputpath_bkgs)
                            WriteList(datasets_signals_ptag, outputpath_signals)
                    else:
                        WriteList(datasets_bkgs, outputpath_bkgs)
                        WriteList(datasets_signals, outputpath_signals)
                    logging.info("Wrote background sample list (for skim derivations) to {o}".format(o=outputpath_bkgs))
                    logging.info("Wrote signal sample list (for noskim derivations) to {o}".format(o=outputpath_signals))

                except Exception as e:
                    logging.warning('Could not write dataset list for {c}'.format(c=campaign))
                    logging.warning(e)

            # data lists
            if not nodatalists:
                for year in self.GRL.keys():
                    try:
                        datasets_data = sorted(ClearFromDuplicates(getattr(self, '_' + aod + '_' + year)))
                        filename_data = year + '_' + aod + '.txt'
                        outputpath_data = os.path.join(self.outputDir, filename_data)
                        if ptags:
                            if aod in self._samples.derivations():
                                datasets_data_ptags = self.checkPTags(datasets_data, ptags)
                                WriteList(datasets_data_ptags, outputpath_data)
                        else:
                            WriteList(datasets_data, outputpath_data)
                        logging.info("Wrote data sample list to {o}".format(o=outputpath_data))

                    except Exception as e:
                        logging.warning('Could not write dataset list for {c}'.format(c=campaign))
                        logging.warning(e)


def main():
    """Create sample lists of DxAODs for signals in samplelist.txt and tags in samplelist_config.json."""
    # get command line arguments
    args = getArguments().parse_args()

    # checks
    CheckAMISetup()
    CheckRucioSetup()
    CheckRemainingProxyTime()
    if not sys.version_info >= (2, 7):
        logging.error('You need at least python version 2.7. Please enter setupATLAS and lsetup python.')
        sys.exit(1)

    # set up verbosity
    logging.info('Creating sample list...')

    # initialise sample list creator and connect to AMI
    slcr = SampleListCreator(args.config)
    slcr.queryAMI(args.appyVetoForAOD)

    # check for completeness
    slcr.checkDataSets()
    if not args.noDataLists:
        slcr.checkGRL(args.skipCheckGRL)

    # write sample lists
    slcr.writeSampleLists(nodatalists=args.noDataLists, ptags=args.pTags)


if __name__ == '__main__':
    main()

#!/usr/bin/env python
import argparse
import os


def ConvertSampleName(rawName):
    dict_rawSub_final = {
        'ttbar': 'ttbar',
        'Wenu': 'Wjets',
        'Wmunu': 'Wjets',
        'Wtaunu': 'Wjets',
        'Zee': 'Zjets',
        'Zmumu': 'Zjets',
        'Ztautau': 'Zjets',
        'Znunu': 'Zjets',
        'SingleTopS': 'SgTopS',
        'singletop_s': 'SgTopS',
        'singletop_t': 'SgTopT',
        'tchan_BW50_lept_top': 'SgTopT',
        'tchan_BW50_lept_antitop': 'SgTopT',
        '_Wt_': 'SgTopWt',
        'ZqqZvv': 'ZZ',
        'ZqqZll': 'ZZ',
        'ZbbZvv': 'ZZ',
        'ZbbZll': 'ZZ',
        'ggZllZqq': 'ZZ',
        'ggZvvZqq': 'ZZ',
        'WqqZvv': 'WZ',
        'WqqZll': 'WZ',
        'WlvZbb': 'WZ',
        'WlvZqq': 'WZ',
        'WpqqWmlv': 'WW',
        'WplvWmqq': 'WW',
        'ggWmlvWpqq': 'WW',
        'ZH': 'VH',
        'WpH': 'VH',
        'WmH': 'VH',
        'jetjet': 'dijet',
        'SinglePhoton': 'photon',
        'zp2hdm': 'zp2hdm'
    }
    for rawNameSub in dict_rawSub_final:
        if rawNameSub.lower() in rawName.lower():
            return dict_rawSub_final[rawNameSub]
    return rawName


parser = argparse.ArgumentParser(description='Collect all mc samples of a given set and assign process names to DSIDs.')
parser.add_argument('--inDir', action='store', dest='inputFileDir', default='XAMPPmonoH/data/SampleLists')
parser.add_argument('--inFileNameTag', action='store', dest='inFileNameTag', default='mc16')
parser.add_argument('--outFileName', action='store', dest='outFileName', default='XAMPPmonoH/data/mc16_dsid_proc.txt')
clArgs = parser.parse_args()

map_dsid_proc = {}

for file in os.listdir(clArgs.inputFileDir):
    if clArgs.inFileNameTag in file:
        inFile = open(clArgs.inputFileDir + '/' + file, 'r')
        for line in inFile:
            dsid = line.split('.')[1]
            rawName = line.split('.')[2]
            if dsid not in map_dsid_proc:
                map_dsid_proc[dsid] = ConvertSampleName(rawName)

outFile = open(clArgs.outFileName, 'w')
for dsid in sorted(map_dsid_proc):
    outFile.write('{} {}\n'.format(dsid, map_dsid_proc[dsid]))
outFile.close()
print 'Written dsid / sample name pairs to', outFile.name

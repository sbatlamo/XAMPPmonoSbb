#include <XAMPPmonoH/AnalysisUtils.h>
#include <XAMPPmonoH/MonoHTruthAnalysisHelper.h>

// Tool includes
#include <PathResolver/PathResolver.h>

#include <fstream>
#include <iostream>

namespace XAMPP {
    static CharDecorator dec_bjet("bjet");
    static IntDecorator dec_NBtags("btag");

    MonoHTruthAnalysisHelper::MonoHTruthAnalysisHelper(std::string myname) : SUSYTruthAnalysisHelper(myname) {
        m_r3 = std::make_unique<TRandom3>(19900101);
    }

    unsigned int MonoHTruthAnalysisHelper::finalState() {
        if (isData()) {
            ATH_MSG_WARNING("The current event is no simulated event");
            return -1;
        }
        // always return 1 (note: SUSY analysis uses this to distinguish background: 0 and signal: 1)
        // note: FS in cross-section file must also be 1 to match this
        return 1;
    }

    bool MonoHTruthAnalysisHelper::isBJet(const xAOD::Jet& jet) const {
        // poor man's b-tagging
        float r = m_r3->Rndm();
        char isB = false;
        static IntAccessor acc_TruthLabel_bjet("HadronConeExclTruthLabelID");
        if (acc_TruthLabel_bjet.isAvailable(jet)) {
            // b-jet
            if (acc_TruthLabel_bjet(jet) == 5 && r < 0.7) isB = true;
            // c-jet (mistag)
            if (acc_TruthLabel_bjet(jet) == 4 && r < 0.12) isB = true;
            // light jet (mistag)
            if (acc_TruthLabel_bjet(jet) < 4 && r < 0.006) isB = true;
        }
        return isB;
    }

    StatusCode MonoHTruthAnalysisHelper::initializeEventVariables() {
        // initialize particle trees
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Muons"));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Electrons"));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Jets", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("FatJets", true));

        ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("Muons");
        ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("Electrons");
        ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("Jets");
        ParticleStorage* FatJetStore = m_XAMPPInfo->GetParticleStorage("FatJets");

        StringVector FloatVarsLeptons{"charge"};
        StringVector FloatVarsJets{};
        StringVector CharVarsLeptons{"signal"};
        StringVector CharVarsJets{"signal", "bjet"};

        StringVector IntVarsFatJets{"btag"};

        ATH_CHECK(MuonStore->SaveVariable<float>(FloatVarsLeptons));
        ATH_CHECK(ElectronStore->SaveVariable<float>(FloatVarsLeptons));
        ATH_CHECK(JetStore->SaveVariable<float>(FloatVarsJets));

        ATH_CHECK(MuonStore->SaveVariable<char>(CharVarsLeptons));
        ATH_CHECK(ElectronStore->SaveVariable<char>(CharVarsLeptons));
        ATH_CHECK(JetStore->SaveVariable<char>(CharVarsJets));

        ATH_CHECK(FatJetStore->SaveVariable<int>(IntVarsFatJets));

        // initialize event variables
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_Lep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_Jets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_FatJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BJets04"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BJetsAssoc02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BJetsNonAssoc02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("LeadingjetPt"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("SubLeadingjetPt"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("abs_mindPhi"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("sigjet012ptsum"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("dphi_jj"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("dphi_metJ"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("dphi_metjj"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("m_J"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("m_jj"));

        return StatusCode::SUCCESS;
    }
    StatusCode MonoHTruthAnalysisHelper::ComputeEventVariables() {
        // muons
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("Muons");
        ATH_CHECK(MuonStore->Fill(m_truth_selection->GetTruthBaselineMuons()));
        // electrons
        static XAMPP::ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("Electrons");
        ATH_CHECK(ElectronStore->Fill(m_truth_selection->GetTruthBaselineElectrons()));
        // jets
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("Jets");
        ATH_CHECK(JetStore->Fill(m_truth_selection->GetTruthSignalJets()));
        // large-R jets
        static XAMPP::ParticleStorage* FatJetStore = m_XAMPPInfo->GetParticleStorage("FatJets");
        ATH_CHECK(FatJetStore->Fill(m_truth_selection->GetTruthFatJets()));

        // event variables
        static XAMPP::Storage<int>* dec_NLepSignal = m_XAMPPInfo->GetVariableStorage<int>("N_Lep");
        static XAMPP::Storage<int>* dec_NJets = m_XAMPPInfo->GetVariableStorage<int>("N_Jets");
        static XAMPP::Storage<int>* dec_NFatJets = m_XAMPPInfo->GetVariableStorage<int>("N_FatJets");
        static XAMPP::Storage<int>* dec_NBJets04 = m_XAMPPInfo->GetVariableStorage<int>("N_BJets04");
        static XAMPP::Storage<int>* dec_NBJetsAssoc02 = m_XAMPPInfo->GetVariableStorage<int>("N_BJetsAssoc02");
        static XAMPP::Storage<int>* dec_NBJetsNonAssoc02 = m_XAMPPInfo->GetVariableStorage<int>("N_BJetsNonAssoc02");
        static XAMPP::Storage<float>* dec_LeadingjetPt = m_XAMPPInfo->GetVariableStorage<float>("LeadingjetPt");
        static XAMPP::Storage<float>* dec_SubLeadingjetPt = m_XAMPPInfo->GetVariableStorage<float>("SubLeadingjetPt");
        static XAMPP::Storage<float>* dec_abs_min_dPhi = m_XAMPPInfo->GetVariableStorage<float>("abs_mindPhi");
        static XAMPP::Storage<float>* dec_sigjet012ptsum = m_XAMPPInfo->GetVariableStorage<float>("sigjet012ptsum");
        static XAMPP::Storage<float>* dec_dphi_jj = m_XAMPPInfo->GetVariableStorage<float>("dphi_jj");
        static XAMPP::Storage<float>* dec_dphi_metJ = m_XAMPPInfo->GetVariableStorage<float>("dphi_metJ");
        static XAMPP::Storage<float>* dec_dphi_metjj = m_XAMPPInfo->GetVariableStorage<float>("dphi_metjj");
        static XAMPP::Storage<float>* dec_m_J = m_XAMPPInfo->GetVariableStorage<float>("m_J");
        static XAMPP::Storage<float>* dec_m_jj = m_XAMPPInfo->GetVariableStorage<float>("m_jj");

        static XAMPP::Storage<XAMPP::XAMPPmet>* dec_MET = m_XAMPPInfo->GetVariableStorage<XAMPP::XAMPPmet>("TruthMET");

        int N_Lep = m_truth_selection->GetTruthSignalElectrons()->size() + m_truth_selection->GetTruthSignalMuons()->size();
        int N_Jets = m_truth_selection->GetTruthSignalJets()->size();
        int N_FatJets = m_truth_selection->GetTruthFatJets()->size();

        // MET
        xAOD::MissingET* met = dec_MET->GetValue();
        TLorentzVector metVec;
        metVec.SetPxPyPzE(met->mpx(), met->mpy(), 0, met->met());

        // small-R jets
        xAOD::JetContainer* truth_jets_smallR = nullptr;
        ATH_CHECK(ViewElementsContainer("truthjets04", truth_jets_smallR));

        int N_BJets04 = 0;
        char isBJet04 = false;
        if (m_truth_selection->GetTruthSignalJets()) {
            for (const auto ijet : *(m_truth_selection->GetTruthSignalJets())) {
                // re-decorate jets with b-tagging result to account for efficiency
                isBJet04 = isBJet(*ijet);
                dec_bjet(*ijet) = isBJet04;
                if (isBJet04) N_BJets04++;
                truth_jets_smallR->push_back(ijet);
            }
        }

        // large-R jets
        xAOD::JetContainer* truth_jets_largeR = nullptr;
        ATH_CHECK(ViewElementsContainer("truthjets10", truth_jets_largeR));
        char isBJet02 = false;
        char isAssociated = false;

        int N_BJetsAssoc02 = 0;
        int N_BJetsNonAssoc02 = 0;

        if (m_truth_selection->GetTruthCustomJets("baselineTrackJets") && m_truth_selection->GetTruthFatJets()) {
            for (const auto ifatjet : *(m_truth_selection->GetTruthFatJets())) {
                int NBtags = 0;
                for (const auto itrackjet : *(m_truth_selection->GetTruthCustomJets("baselineTrackJets"))) {
                    // dR association of trackjets to large-R jet
                    isAssociated = (xAOD::P4Helpers::deltaR(ifatjet, itrackjet) <= 1.1);
                    // decorate track jet with b-tagging information
                    isBJet02 = isBJet(*itrackjet);
                    if (isBJet02 && isAssociated) {
                        NBtags++;
                        N_BJetsAssoc02++;
                    } else if (isBJet02 && !isAssociated) {
                        N_BJetsNonAssoc02++;
                    }
                }  // end trackjet loop
                dec_NBtags(*ifatjet) = NBtags;
                truth_jets_largeR->push_back(ifatjet);
                break;  // only process leading large-R jet (similar to MonoHAnalysisHelper)
            }           // end large-R jet loop
        }               // end if

        // event variables
        float m_J = -1;
        float dphi_metJ = -1;
        if (m_truth_selection->GetTruthFatJets()->size() > 0) {
            m_J = m_truth_selection->GetTruthFatJets()->at(0)->p4().M();
            dphi_metJ = fabs(metVec.DeltaPhi(m_truth_selection->GetTruthFatJets()->at(0)->p4()) * (180 / TMath::Pi()));  // > 120.0
        }

        float LeadingjetPt = -1.;
        if (truth_jets_smallR->size() > 0) LeadingjetPt = truth_jets_smallR->at(0)->pt();
        float SubLeadingjetPt = -1.;
        float m_jj = -1;
        if (truth_jets_smallR->size() > 1) {
            m_jj = (truth_jets_smallR->at(0)->p4() + truth_jets_smallR->at(1)->p4()).M();
            SubLeadingjetPt = truth_jets_smallR->at(1)->pt();
        }

        float abs_mindPhi = fabs(ComputeDPhiMin(m_truth_selection->GetTruthBaselineJets(), met, 3));

        float sigjet012ptsum = 0.0;
        if (truth_jets_smallR->size() == 2) {
            sigjet012ptsum = truth_jets_smallR->at(0)->p4().Pt() + truth_jets_smallR->at(1)->p4().Pt();
        } else if (truth_jets_smallR->size() > 2) {
            sigjet012ptsum =
                truth_jets_smallR->at(0)->p4().Pt() + truth_jets_smallR->at(1)->p4().Pt() + truth_jets_smallR->at(2)->p4().Pt();
        }

        float dphi_jj = -1;
        if (truth_jets_smallR->size() >= 2)
            dphi_jj = fabs(truth_jets_smallR->at(0)->p4().DeltaPhi(truth_jets_smallR->at(1)->p4()) * (180 / TMath::Pi()));  // < 140

        float dphi_metjj = -1;
        if (met && truth_jets_smallR->size() >= 2)
            dphi_metjj =
                fabs(metVec.DeltaPhi(truth_jets_smallR->at(0)->p4() + truth_jets_smallR->at(1)->p4()) * (180 / TMath::Pi()));  // > 120.0

        ATH_CHECK(dec_NLepSignal->Store(N_Lep));
        ATH_CHECK(dec_NJets->Store(N_Jets));
        ATH_CHECK(dec_NFatJets->Store(N_FatJets));
        ATH_CHECK(dec_NBJets04->Store(N_BJets04));
        ATH_CHECK(dec_NBJetsAssoc02->Store(N_BJetsAssoc02));
        ATH_CHECK(dec_NBJetsNonAssoc02->Store(N_BJetsNonAssoc02));
        ATH_CHECK(dec_m_J->Store(m_J));
        ATH_CHECK(dec_m_jj->Store(m_jj));
        ATH_CHECK(dec_LeadingjetPt->Store(LeadingjetPt));
        ATH_CHECK(dec_SubLeadingjetPt->Store(SubLeadingjetPt));
        ATH_CHECK(dec_abs_min_dPhi->Store(abs_mindPhi));
        ATH_CHECK(dec_sigjet012ptsum->Store(sigjet012ptsum));
        ATH_CHECK(dec_dphi_jj->Store(dphi_jj));
        ATH_CHECK(dec_dphi_metJ->Store(dphi_metJ));
        ATH_CHECK(dec_dphi_metjj->Store(dphi_metjj));

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP

#include "XAMPPmonoH/MonoHTruthSelector.h"

#include <algorithm>
#include <iostream>

#include "XAMPPbase/AnalysisUtils.h"

namespace XAMPP {

    MonoHTruthSelector::MonoHTruthSelector(std::string myName) :
        SUSYTruthSelector(myName),
        m_FatJetDefs(),
        m_PreFatJets(nullptr),
        m_BaselineFatJets(nullptr),
        m_SignalFatJets(nullptr),
        m_TrackJetDefs(),
        m_InitialTrackJets(nullptr),
        m_PreTrackJets(nullptr),
        m_BaselineTrackJets(nullptr),
        m_SignalTrackJets(nullptr) {
        declare(m_FatJetDefs, "FatJet");
        declare(m_TrackJetDefs, "TrackJet");
    }
    //___________________________________________________________________________

    StatusCode MonoHTruthSelector::initialize() {
        // initialize parent class
        ATH_CHECK(SUSYTruthSelector::initialize());

        if (doTruthJets()) {
            // in addition initialize large-R jets + track jets
            if (doTruthFatJets()) ATH_CHECK(init(m_FatJetDefs, "FatJet"));
            if (doTruthTrackJets()) ATH_CHECK(init(m_TrackJetDefs, "TrackJet"));
        }

        return StatusCode::SUCCESS;
    }

    //___________________________________________________________________________

    StatusCode MonoHTruthSelector::InitialFill(const CP::SystematicSet& systset) {
        // initialize parent class
        ATH_CHECK(SUSYTruthSelector::InitialFill(systset));

        if (doTruthJets()) {
            // large-R jets
            if (doTruthFatJets()) {
                if (CreateContainerLinks(FatJetKey(), m_InitialFatJets) == LinkStatus::Failed) return StatusCode::FAILURE;
            } else
                ATH_CHECK(ViewElementsContainer("PreInitFatJets", m_InitialFatJets));

            ATH_CHECK(ViewElementsContainer("preFatJets", m_PreFatJets));
            ATH_CHECK(ViewElementsContainer("baselineFatJets", m_BaselineFatJets));
            ATH_CHECK(ViewElementsContainer("signalFatJets", m_SignalFatJets));

            InitContainer(m_InitialFatJets, m_PreFatJets);

            // track jets
            if (doTruthTrackJets()) {
                if (CreateContainerLinks(TrackJetKey(), m_InitialTrackJets) == LinkStatus::Failed) return StatusCode::FAILURE;
            } else
                ATH_CHECK(ViewElementsContainer("PreInitTrackJets", m_InitialTrackJets));

            ATH_CHECK(ViewElementsContainer("preTrackJets", m_PreTrackJets));
            ATH_CHECK(ViewElementsContainer("baselineTrackJets", m_BaselineTrackJets));
            ATH_CHECK(ViewElementsContainer("signalTrackJets", m_SignalTrackJets));

            InitContainer(m_InitialTrackJets, m_PreTrackJets);
        }

        return StatusCode::SUCCESS;
    }

    //___________________________________________________________________________

    StatusCode MonoHTruthSelector::FillTruth(const CP::SystematicSet& systset) {
        // initialize parent class
        ATH_CHECK(SUSYTruthSelector::FillTruth(systset));

        if (doTruthJets()) {
            // large-R jets
            if (doTruthFatJets()) {
                for (const auto& truj_it : *m_PreFatJets) {
                    if (!PassBaseline(*truj_it)) continue;
                    m_BaselineFatJets->push_back(truj_it);
                    if (!PassSignal(*truj_it)) continue;
                    m_SignalFatJets->push_back(truj_it);
                }
            }

            // track jets
            if (doTruthTrackJets()) {
                for (const auto& truj_it : *m_PreTrackJets) {
                    if (!PassBaseline(*truj_it)) continue;
                    m_BaselineTrackJets->push_back(truj_it);
                    if (!PassSignal(*truj_it)) continue;
                    m_SignalTrackJets->push_back(truj_it);
                }
            }
        }

        return StatusCode::SUCCESS;
    }

    //___________________________________________________________________________

    StatusCode MonoHTruthSelector::LoadContainers() {
        ATH_CHECK(SUSYTruthSelector::LoadContainers());

        if (doTruthParticles()) {
            // Now fill W, Z, top containers.
            // Depending on the degree of messiness of
            // the input file some handcraft might be needed here.
            //
            m_truthWbosonMomenta.clear();
            m_truthZbosonMomenta.clear();
            m_truthTopQuarkMomenta.clear();

            for (const xAOD::TruthParticle* truthPrt : *m_xAODTruthParticles) {
                if (IsBremsProduct(*truthPrt)) continue;
                if (truthPrt->isW()) { m_truthWbosonMomenta.push_back(truthPrt->p4()); }
                if (truthPrt->isZ()) m_truthZbosonMomenta.push_back(truthPrt->p4());
                if (truthPrt->isTop()) { m_truthTopQuarkMomenta.push_back(truthPrt->p4()); }
            }
            if (m_truthZbosonMomenta.empty() && m_truthWbosonMomenta.empty() && m_truthTopQuarkMomenta.empty()) {
                // Search for Z -> nunu
                ATH_CHECK(ConstructMotherOfPrimaryDecay({{12, -12}, {14, -14}, {16, -16}}, m_truthZbosonMomenta));
                // Search for Z -> ll
                ATH_CHECK(ConstructMotherOfPrimaryDecay({{11, -11}, {13, -13}, {15, -15}}, m_truthZbosonMomenta));
                // Search for W^{+/-} -> lnu
                ATH_CHECK(ConstructMotherOfPrimaryDecay({{11, -12}, {13, -14}, {15, -16}, {-11, 12}, {-13, 14}, {-15, 16}},
                                                        m_truthWbosonMomenta));
            }
        }
        return StatusCode::SUCCESS;
    }

    //___________________________________________________________________________

    StatusCode MonoHTruthSelector::ConstructMotherOfPrimaryDecay(const std::vector<std::pair<int, int> >&& daughterPdgIdPairs,
                                                                 FourMomentumContainer& motherFourMomenta) {
        // Get the four momentum of a heavy particle which decayed into
        // a pair of particles provided by the pdg id pairs.
        //
        // Function fails if there are ambiguities, e.g.
        // in ZZ->llll events.
        //
        // In principle one could make use of the pdg id of the
        // mother particle, but at least in some DxAODs the
        // decay chains seem to be messed up somewhat, so
        // it's not clear if that's always safe. PR.
        TLorentzVector motherFourMomentum;
        unsigned int nDaughters = 0;
        int daughterPdgId_2 = 0;
        std::vector<std::pair<int, int> >::const_iterator itrDaugtherPdgIdPairs = daughterPdgIdPairs.begin();
        for (const xAOD::TruthParticle* truthPrt : *m_xAODTruthParticles) {
            if (nDaughters == 0) {
                std::vector<std::pair<int, int> >::const_iterator itrDaugtherPdgIdPairs = daughterPdgIdPairs.begin();
                for (; itrDaugtherPdgIdPairs != daughterPdgIdPairs.end(); ++itrDaugtherPdgIdPairs) {
                    if (truthPrt->pdgId() != itrDaugtherPdgIdPairs->first && truthPrt->pdgId() != itrDaugtherPdgIdPairs->second)
                        continue;
                    else {
                        if (IsFromSecondaryDecay(*truthPrt)) continue;
                        daughterPdgId_2 = (truthPrt->pdgId() == itrDaugtherPdgIdPairs->first ? itrDaugtherPdgIdPairs->second
                                                                                             : itrDaugtherPdgIdPairs->first);
                        motherFourMomentum += truthPrt->p4();
                        ++nDaughters;
                    }
                }
            } else if (truthPrt->pdgId() == daughterPdgId_2 && !IsFromSecondaryDecay(*truthPrt)) {
                motherFourMomentum += truthPrt->p4();
                ++nDaughters;
                break;
            }
        }
        if (nDaughters != 2) return StatusCode::SUCCESS;

        motherFourMomenta.push_back(motherFourMomentum);

        return StatusCode::SUCCESS;
    }
    //___________________________________________________________________________

    bool MonoHTruthSelector::IsFromSecondaryDecay(const xAOD::TruthParticle& truthPrt) {
        if (truthPrt.nParents()) {
            if (truthPrt.parent(0)) {
                if (truthPrt.parent(0)->isElectron() ||  // sometimes particles are
                                                         // attached to themselves
                    truthPrt.parent(0)->isMuon() || truthPrt.parent(0)->isTau() || truthPrt.parent(0)->isNeutrino() ||
                    truthPrt.parent(0)->isPhoton() || truthPrt.parent(0)->isHadron())
                    return true;
            }
        }
        return false;
    }

    //___________________________________________________________________________

    bool MonoHTruthSelector::IsBremsProduct(const xAOD::TruthParticle& truthPrt) {
        for (unsigned int iParent = 0; iParent < truthPrt.nParents(); ++iParent) {
            if (!truthPrt.parent(iParent)) continue;
            if (truthPrt.parent(iParent)->pdgId() == truthPrt.pdgId()) return true;
        }
        return false;
    }

    //___________________________________________________________________________

    bool MonoHTruthSelector::doTruthFatJets() const { return m_FatJetDefs.doObject; }
    std::string MonoHTruthSelector::FatJetKey() const { return m_FatJetDefs.ContainerKey; }

    bool MonoHTruthSelector::doTruthTrackJets() const { return m_TrackJetDefs.doObject; }
    std::string MonoHTruthSelector::TrackJetKey() const { return m_TrackJetDefs.ContainerKey; }

    /////////////////////////////////////////
    // classification of particle decay modes
    /////////////////////////////////////////

    int MonoHTruthSelector::classifyWDecays(const xAOD::TruthParticle* particle) {
        if (!particle->isW() || particle->isGenSpecific()) return WDecayModes::Unspecified;
        // variables defined to count the number of possible W decay products: Quarks, charged leptons, taus and neutrinos
        unsigned int nq(0), ne(0), nm(0), nthad(0), nte(0), ntmu(0), nnu(0);
        // Find the W children
        for (size_t c = 0; c < particle->nChildren(); ++c) {
            const xAOD::TruthParticle* child = particle->child(c);
            if (!child) continue;
            // in case that the W decays into itself call this function again
            if (child->isW())
                return classifyWDecays(child);
            else if (child->isQuark())
                ++nq;
            else if (child->isElectron())
                ++ne;
            else if (child->isMuon())
                ++nm;
            else if (child->isTau())
                if (classifyTauDecays(child) == tauDecayModes::ElecTau)
                    ++nte;
                else if (classifyTauDecays(child) == tauDecayModes::MuonTau)
                    ++ntmu;
                else if (classifyTauDecays(child) == tauDecayModes::HadTau)
                    ++nthad;
                else
                    continue;
            else if (child->isNeutrino())
                ++nnu;
        }
        if (nq == 2) return WDecayModes::Hadronic;
        if (ne == 1 && nnu == 1) return WDecayModes::ElecNeut;
        if (nm == 1 && nnu == 1) return WDecayModes::MuonNeut;
        if (nte == 1 && nnu == 1) return WDecayModes::ElecTauNeut;
        if (ntmu == 1 && nnu == 1) return WDecayModes::MuonTauNeut;
        if (nthad == 1 && nnu == 1)
            return WDecayModes::HadTauNeut;
        else
            // if nothing from above true --> W decay cannot be specified
            return WDecayModes::Unspecified;
    }

    int MonoHTruthSelector::classifyTopDecays(const xAOD::TruthParticle* particle) {
        int topDecay = WDecayModes::Unspecified;
        // check if parsed top is a valid truth top quark
        if (!isTrueTop(particle)) return topDecay;
        for (size_t i = 0; i < particle->nChildren(); i++) {
            const xAOD::TruthParticle* child = particle->child(i);
            if (!child) continue;
            // in case that the top decays into itself call this function again
            if (child->isTop()) return classifyTopDecays(child);
            // top decay modes are classified through the W decay modes
            if (child->isW()) topDecay = classifyWDecays(child);
        }
        return topDecay;
    }

    int MonoHTruthSelector::classifyTauDecays(const xAOD::TruthParticle* particle) {
        int tauDecay = tauDecayModes::TauUnknown;
        if (!particle->isTau() || particle->isGenSpecific()) return tauDecay;
        // variables defined to count the number of possible tau decay products: Quarks, charged leptons and neutrinos
        unsigned int nq(0), ne(0), nmu(0), nnu(0);
        if (particle->nChildren() > 1) {
            for (size_t c = 0; c < particle->nChildren(); ++c) {
                const xAOD::TruthParticle* child = particle->child(c);
                if (!child) continue;
                if (child->isElectron()) {
                    ++ne;
                } else if (child->isMuon()) {
                    ++nmu;
                } else if (child->isNeutrino()) {
                    ++nnu;
                }
                // Calling this function again when tau decays into itself as in tau -> tau+photon
                else if (child->isTau())
                    return classifyTauDecays(child);
                // if nothing from above true --> hadronic tau decay
                else
                    ++nq;
            }
        }
        if (nnu == 1 && nq > 0) tauDecay = tauDecayModes::HadTau;
        if (nnu == 2 && ne == 1) tauDecay = tauDecayModes::ElecTau;
        if (nnu == 2 && nmu == 1) tauDecay = tauDecayModes::MuonTau;
        return tauDecay;
    }

    bool MonoHTruthSelector::isCombinedDecay(int topDecay, int antitopDecay, int d1, int d2) {
        return ((topDecay == d1 && antitopDecay == d2) || (topDecay == d2 && antitopDecay == d1));
    }

    int MonoHTruthSelector::getReducedWDecay(int topDecay) {
        int reducedTopDecay;
        if (topDecay == WDecayModes::Hadronic)
            reducedTopDecay = reducedWDecayModes::rHadronic;
        else if (topDecay == WDecayModes::ElecNeut || topDecay == WDecayModes::MuonNeut)
            reducedTopDecay = reducedWDecayModes::rLepNeut;
        else if (topDecay == WDecayModes::ElecTauNeut || topDecay == WDecayModes::MuonTauNeut)
            reducedTopDecay = reducedWDecayModes::rLepTauNeut;
        else if (topDecay == WDecayModes::HadTauNeut)
            reducedTopDecay = reducedWDecayModes::rHadTauNeut;
        else if (topDecay == WDecayModes::Unspecified)
            reducedTopDecay = reducedWDecayModes::rUnspecified;
        return reducedTopDecay;
    }

    int MonoHTruthSelector::combineDecayModes(int reducedtopDecay, int reducedantitopDecay) {
        int ttbarCat;
        if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepNeut, reducedWDecayModes::rLepNeut))
            ttbarCat = ttbarDecayModes::Lep_Lep;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepNeut, reducedWDecayModes::rHadronic))
            ttbarCat = ttbarDecayModes::Lep_Had;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepNeut, reducedWDecayModes::rHadTauNeut))
            ttbarCat = ttbarDecayModes::Lep_HadTau;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepNeut, reducedWDecayModes::rLepTauNeut))
            ttbarCat = ttbarDecayModes::Lep_LepTau;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepTauNeut, reducedWDecayModes::rHadronic))
            ttbarCat = ttbarDecayModes::LepTau_Had;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepTauNeut, reducedWDecayModes::rHadTauNeut))
            ttbarCat = ttbarDecayModes::LepTau_HadTau;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rLepTauNeut, reducedWDecayModes::rLepTauNeut))
            ttbarCat = ttbarDecayModes::LepTau_LepTau;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rHadronic, reducedWDecayModes::rHadronic))
            ttbarCat = ttbarDecayModes::Had_Had;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rHadronic, reducedWDecayModes::rHadTauNeut))
            ttbarCat = ttbarDecayModes::Had_HadTau;
        else if (isCombinedDecay(reducedtopDecay, reducedantitopDecay, reducedWDecayModes::rHadTauNeut, reducedWDecayModes::rHadTauNeut))
            ttbarCat = ttbarDecayModes::HadTau_HadTau;
        else
            ttbarCat = ttbarDecayModes::Unknown;
        return ttbarCat;
    }

    int MonoHTruthSelector::classifyTtbarDecays(const xAOD::TruthParticle* top, const xAOD::TruthParticle* antitop) {
        int topDecay = WDecayModes::Unspecified;
        int antitopDecay = WDecayModes::Unspecified;
        topDecay = classifyTopDecays(top);
        antitopDecay = classifyTopDecays(antitop);
        int reducedtopDecay = getReducedWDecay(topDecay);
        int reducedantitopDecay = getReducedWDecay(antitopDecay);
        int ttbarCat = combineDecayModes(reducedtopDecay, reducedantitopDecay);
        return ttbarCat;
    }

}  // namespace XAMPP

#include "XAMPPmonoH/JetTruthAssociationModule.h"

#include "xAODTruth/TruthParticleContainer.h"

namespace {
    template <typename Iterator> Iterator closestJet(Iterator begin, Iterator end, TLorentzVector targetP4) {
        return std::min_element(begin, end, [p4 = targetP4](const xAOD::Jet* lhs, const xAOD::Jet* rhs) {
            return p4.DeltaR(lhs->p4()) < p4.DeltaR(rhs->p4());
        });
    }
}  // namespace

namespace XAMPP {
    JetTruthAssociationModule::JetTruthAssociationModule(const std::string& name) : asg::AsgTool(name), m_evtInfoHdl("EventInfoHandler") {
        declareProperty("JetSelector", m_jetSelector, "The jet selector");
        declareProperty("TruthJetCollection", m_truthJetName = "AntiKt4TruthJets", "The input truth jets");
        declareProperty("TruthLRJetCollection", m_truthLRJetName = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets", "The input truth R=1.0 jets");
        declareProperty("TruthParticleCollection", m_truthParticlesName = "TruthParticles", "The input truth particles");
        declareProperty("DR", m_DR = 0.2, "The maximum DR between truth and reco jets to match");
        declareProperty("LRDR", m_LRDR = 0.4, "The maximum DR between truth and reco large-r jets to match");
    }

    JetTruthAssociationModule::~JetTruthAssociationModule() {}

    StatusCode JetTruthAssociationModule::initialize() {
        ATH_CHECK(m_jetSelector.retrieve());
        ATH_CHECK(m_evtInfoHdl.retrieve());

        m_XAMPPInfo = dynamic_cast<XAMPP::EventInfo*>(m_evtInfoHdl.get());
        return StatusCode::SUCCESS;
    }

    StatusCode JetTruthAssociationModule::bookVariables() {
        ATH_MSG_INFO("Booking variables");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "Jet1Truth_", ""));
        m_j1Truth = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "Jet1Truth_", "");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "Jet1TruthCorr_", ""));
        m_j1TruthCorr = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "Jet1TruthCorr_", "");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "Jet2Truth_", ""));
        m_j2Truth = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "Jet2Truth_", "");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "Jet2TruthCorr_", ""));
        m_j2TruthCorr = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "Jet2TruthCorr_", "");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "FatJetTruth_", ""));
        m_lrjTruth = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "FatJetTruth_", "");
        ATH_CHECK(P4XAMPPOutput::createBranches(m_XAMPPInfo, "FatJetTruthCorr_", ""));
        m_lrjTruthCorr = std::make_unique<P4XAMPPOutput>(m_XAMPPInfo, "FatJetTruthCorr_", "");
        return StatusCode::SUCCESS;
    }

    StatusCode JetTruthAssociationModule::fill() {
        const xAOD::JetContainer* truthJets = nullptr;
        ATH_CHECK(evtStore()->retrieve(truthJets, m_truthJetName));

        const xAOD::JetContainer* truthLRJets = nullptr;
        ATH_CHECK(evtStore()->retrieve(truthLRJets, m_truthLRJetName));

        const xAOD::TruthParticleContainer* truth = nullptr;
        ATH_CHECK(evtStore()->retrieve(truth, m_truthParticlesName));

        // Correct the jet collections
        std::map<const xAOD::Jet*, TLorentzVector> truthJetsCorr = correctedJetP4s(truthJets, truth, 0.4);
        std::map<const xAOD::Jet*, TLorentzVector> truthLRJetsCorr = correctedJetP4s(truthLRJets, truth, 1.0);

        // Now get the relevant jets from the selector
        const xAOD::JetContainer* btagLightJets = m_jetSelector->GetCustomJets("btaggedLight");
        const xAOD::JetContainer* lrJets = m_jetSelector->GetBaselineJets(10);

        // Set all the variables to unphysical defaults
        m_j1Truth->setP4(TLorentzVector{});
        m_j1TruthCorr->setP4(TLorentzVector{});
        m_j2Truth->setP4(TLorentzVector{});
        m_j2TruthCorr->setP4(TLorentzVector{});
        m_lrjTruth->setP4(TLorentzVector{});
        m_lrjTruthCorr->setP4(TLorentzVector{});

        if (btagLightJets->size() > 0) {
            // Truth match the first two b-tagged light jets
            // We'll do this with a simple DR match, removing the truth jet matched to
            // the leading BTL jet. There are more intelligent ways of doing this but
            // they aren't worth the effort of implementing them for this sort of study,
            // the edge cases are very rare.
            std::vector<const xAOD::Jet*> truthJetsVec(truthJets->begin(), truthJets->end());
            auto closest = closestJet(truthJetsVec.begin(), truthJetsVec.end(), btagLightJets->at(0)->p4());
            if (closest != truthJetsVec.end()) {
                const xAOD::Jet* matchedJet = *closest;
                if (btagLightJets->at(0)->p4().DeltaR(matchedJet->p4()) < m_DR) {
                    m_j1Truth->setP4(matchedJet->p4());
                    m_j1TruthCorr->setP4(truthJetsCorr.at(matchedJet));
                    truthJetsVec.erase(closest);
                }
            }
            if (btagLightJets->size() > 1) {
                closest = closestJet(truthJetsVec.begin(), truthJetsVec.end(), btagLightJets->at(1)->p4());
                if (closest != truthJetsVec.end()) {
                    const xAOD::Jet* matchedJet = *closest;
                    if (btagLightJets->at(1)->p4().DeltaR(matchedJet->p4()) < m_DR) {
                        m_j2Truth->setP4(matchedJet->p4());
                        m_j2TruthCorr->setP4(truthJetsCorr.at(matchedJet));
                    }
                }
            }
        }

        // Now do the same for the large-r jets
        if (lrJets->size() > 0) {
            auto closest = closestJet(truthLRJets->begin(), truthLRJets->end(), lrJets->at(0)->p4());
            if (closest != truthLRJets->end()) {
                const xAOD::Jet* matchedJet = *closest;
                if (lrJets->at(0)->p4().DeltaR(matchedJet->p4()) < m_LRDR) {
                    m_lrjTruth->setP4(matchedJet->p4());
                    m_lrjTruthCorr->setP4(truthLRJetsCorr.at(matchedJet));
                }
            }
        }
        ATH_CHECK(m_j1Truth->push());
        ATH_CHECK(m_j1TruthCorr->push());
        ATH_CHECK(m_j2Truth->push());
        ATH_CHECK(m_j2TruthCorr->push());
        ATH_CHECK(m_lrjTruth->push());
        ATH_CHECK(m_lrjTruthCorr->push());

        return StatusCode::SUCCESS;
    }

    std::map<const xAOD::Jet*, TLorentzVector> JetTruthAssociationModule::correctedJetP4s(
        const xAOD::JetContainer* jets, const xAOD::TruthParticleContainer* truthParticles, float dr) {
        // Muon/neutrino PDG IDs
        static std::set<int> keepIDs{12, 13, 14, 16, 18};
        // Map jets to their corrected 4-vectors
        std::map<const xAOD::Jet*, TLorentzVector> corrections;
        for (const xAOD::Jet* ijet : *jets) corrections[ijet] = ijet->p4();
        for (const xAOD::TruthParticle* part : *truthParticles) {
            // Skip any that aren't status == 1
            if (part->status() != 1) continue;
            // Skip any that aren't muons or neutrinos
            if (!keepIDs.count(abs(part->pdgId()))) continue;
            // Find the closest jet
            auto closest = closestJet(jets->begin(), jets->end(), part->p4());
            // If none was found or the one found is too far away skip this
            if (closest == jets->end() || part->p4().DeltaR((*closest)->p4()) > dr) continue;
            // Store the corrected p4 to add.
            corrections[*closest] += part->p4();
        }
        return corrections;
    }
}  // namespace XAMPP

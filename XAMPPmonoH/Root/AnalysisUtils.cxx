#include <XAMPPbase/EventInfo.h>
#include <XAMPPmonoH/AnalysisUtils.h>

namespace XAMPP {
    const xAOD::Jet* GetParentJet(const xAOD::Jet* jet) {
        if (jet != nullptr) return GetParentJet(*jet);
        return nullptr;
    }
    const xAOD::Jet* GetParentJet(const xAOD::Jet& jet) {
        static SG::AuxElement::ConstAccessor<ElementLink<xAOD::JetContainer> > acc_parent("Parent");
        if (!acc_parent.isAvailable(jet)) { return nullptr; }
        if (!acc_parent(jet).isValid()) { return nullptr; }
        return (*acc_parent(jet));
    }
    TLorentzVector GetTLV(xAOD::JetFourMom_t tmp) {
        TLorentzVector tlv;
        tlv.SetPx(tmp.px());
        tlv.SetPy(tmp.py());
        tlv.SetPz(tmp.pz());
        tlv.SetE(tmp.e());
        return tlv;
    }
    xAOD::JetFourMom_t GetJetFourMom(TLorentzVector tmp) {
        xAOD::JetFourMom_t jetVec;
        jetVec.SetPt(tmp.Pt());
        jetVec.SetEta(tmp.Eta());
        jetVec.SetPhi(tmp.Phi());
        jetVec.SetM(tmp.M());
        return jetVec;
    }

    StatusCode P4XAMPPOutput::createBranches(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix) {
        for (const std::string& varBase : {"pt", "eta", "phi", "m"})
            if (!evtInfo->NewEventVariable<float>(prefix + varBase + postfix).isSuccess()) return StatusCode::FAILURE;
        return StatusCode::SUCCESS;
    }

    P4XAMPPOutput::P4XAMPPOutput(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix) :
        m_storePt(evtInfo->GetVariableStorage<float>(prefix + "pt" + postfix)),
        m_storeEta(evtInfo->GetVariableStorage<float>(prefix + "eta" + postfix)),
        m_storePhi(evtInfo->GetVariableStorage<float>(prefix + "phi" + postfix)),
        m_storeM(evtInfo->GetVariableStorage<float>(prefix + "m" + postfix)) {}

    StatusCode P4XAMPPOutput::push() {
        ATH_CHECK(m_storePt->Store(m_p4.Pt()));
        ATH_CHECK(m_storeEta->Store(m_p4.Eta()));
        ATH_CHECK(m_storePhi->Store(m_p4.Phi()));
        ATH_CHECK(m_storeM->Store(m_p4.M()));
        return StatusCode::SUCCESS;
    }

    int PCS(double MV2c10) {
        if (MV2c10 < 0.11) {
            return default_score;
        } else if (MV2c10 < 0.64) {
            return wp_85;
        } else if (MV2c10 < 0.83) {
            return wp_77;
        } else if (MV2c10 < 0.94) {
            return wp_70;
        } else {
            return wp_60;
        }
        return default_score;
    }
    int PCS_tj(double MV2c10) {
        if (MV2c10 < 0.05) {
            return default_score;
        } else if (MV2c10 < 0.58) {
            return wp_85;
        } else if (MV2c10 < 0.79) {
            return wp_77;
        } else if (MV2c10 < 0.92) {
            return wp_70;
        } else {
            return wp_60;
        }
        return default_score;
    }
}  // namespace XAMPP

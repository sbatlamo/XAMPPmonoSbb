#!/bin/bash

echo "Updating and re-compiling XAMPPmonoH"
echo "------------------------------------"
echo "Parsing README.md file to create update script..."
grep "#&" "README.md" | sed -r 's/#&//g' | sed -e 's/^[ \t]*//' > UPDATE
echo "Done! Created update script here: ${PWD}/UPDATE"
cat UPDATE
echo "----"
echo "source UPDATE"
source UPDATE && rm UPDATE

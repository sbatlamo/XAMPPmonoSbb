======================
Running the framework
======================

-----------------------
Running locally
-----------------------

.. autofunction:: XAMPPmonoH.runAthena.runAthena

This script will launch a local run of the XAMPPmonoH code. Usually this is only done for testing during development or to check if the settings are correct before launching a production on the grid.

The three most useful options here are

- ``--testJob``: runs a test job (only PRW info for sample is loaded) without systematics
- ``--testJobWithSyst``: runs a test job (only PRW info for sample is loaded) with systematics
- ``--nevents``: specifies number of events to be processed

.. argparse::
   :module: XAMPPmonoH.runAthena
   :func: getArgs
   :prog: runAthena

-----------------------
Running on the Grid
-----------------------

.. autofunction:: XAMPPmonoH.SubmitToGrid.SubmitToGrid

This script will launch a grid production. Usually this is done to produce a new set of ntuples for all samples listed on the sample lists in https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/tree/master/XAMPPmonoH/data/SampleLists

Before launching the script make sure that you have set up `panda` and `rucio` and have obtained a valid grid proxy::

    setupATLAS
    lsetup rucio
    lsetup panda
    voms-proxy-init -voms atlas

Please have a look in the section about ntuple production: :ref:`ntuple-label`

.. argparse::
   :module: XAMPPmonoH.SubmitToGrid
   :func: setupGridSubmitArgParser
   :prog: SubmitToGrid

--------------------------
Running on a batch system
--------------------------

.. autofunction:: XAMPPbase.SubmitToBatch.main

This script will launch a production on a local batch system. This only makes sense if you want to process TRUTH DxAODs or you have the DxAODs required for production on some local group disk to which your batch jobs have access.

In order to be able to process the files it is expected that you have a folder::

    XAMPPmonoH/data/GroupDiskLists/MPPMU_LOCALGROUPDISK

or similar avaliable, where `MPPMU_LOCALGROUPDISK` is the name of your RSE disk. This folder should contain a text file for each sample that list the paths to all files that are associated to this sample. You will have to specify the project (in this case always XAMPPmonoH using the argument ``--BaseProject``) to ensure that the right directory is scanned for the group disk lists.

Example::

    XAMPPmonoH/data/GroupDiskLists/MPPMU_LOCALGROUPDISK/600041.MadGraphPy8EG_A14NNP23LO_monoSbb_zp500_dm100_dh70q0p01x1p0.txt

contains all DxAOD ROOT files associated to the sample ``600041.MadGraphPy8EG_A14NNP23LO_monoSbb_zp500_dm100_dh70q0p01x1p0``.

To run the batch jobs you will in addition have to specify the batch system using the argument ``--engine``.

Example command to process all sample lists whose DSID starts with 600::

    python XAMPPbase/XAMPPbase/python/SubmitToBatch.py \
    --engine SLURM --jobName monoSbb_grid --BaseProject XAMPPmonoH \
    --jobOptions XAMPPmonoH/runTruth.py  \
    --inputDS `ls XAMPPmonoH/data/GroupDiskLists/MPPMU_LOCALGROUPDISK/700* | xargs -n 1 basename | sed -e 's/\.txt*$//'`


.. argparse::
   :module: XAMPPbase.SubmitToBatch
   :func: setupSubmitParser
   :prog: SubmitToBatch

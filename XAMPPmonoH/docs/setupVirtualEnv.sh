#!/bin/bash

# setup python virtualenv
wget https://pypi.python.org/packages/d4/0c/9840c08189e030873387a73b90ada981885010dd9aea134d6de30cd24cb8/virtualenv-15.1.0.tar.gz
tar xvf virtualenv-15.1.0.tar.gz
python2 virtualenv-15.1.0/virtualenv.py ../venv
rm -r virtualenv-15.1.0  virtualenv-15.1.0.tar.gz

# activate virtual environment
source ../venv/bin/activate

# reinstall + update pip
wget https://bootstrap.pypa.io/get-pip.py
../venv/bin/python get-pip.py --force-reinstall
rm get-pip.py

# install requirements
../venv/bin/python -m pip install -r requirements.txt

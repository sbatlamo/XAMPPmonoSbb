import argparse
import os

# Include the common library instead of importing it.
# This allows for full access to the globals.
include("XAMPPmonoH/MonoHToolSetup.py")
AssembleIO()

SetupMonoHTruthAnalysisHelper()
xsecdir = "XAMPPplotting/xSecFiles/MonoH/"
SetupMonoHTruthAnalysisHelper().STCrossSectionDB = xsecdir

SetupMonoHTruthSelector()

##
## CONTAINERS
##
SetupMonoHTruthSelector().ElectronContainer = "TruthElectrons"
SetupMonoHTruthSelector().MuonContainer = "TruthMuons"
SetupMonoHTruthSelector().TauContainer = "TruthTaus"
SetupMonoHTruthSelector().NeutrinoContainer = "TruthNeutrinos"
SetupMonoHTruthSelector().JetContainer = "AntiKt4TruthDressedWZJets"
SetupMonoHTruthSelector().FatJetContainer = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets"
SetupMonoHTruthSelector().TrackJetContainer = "AntiKt2TruthChargedJets"

##
## GENERAL settings
##
# TRUTH3 setup: no TruthParticles container avaliable
if isTRUTH3():
    SetupMonoHTruthSelector().doTruthParticles = False
    SetupMonoHTruthSelector().isTRUTH3 = True
else:
    SetupMonoHTruthSelector().doTruthParticles = True
    SetupMonoHTruthSelector().isTRUTH3 = False
SetupMonoHTruthSelector().rejectUnknownOrigin = True
SetupMonoHTruthSelector().BaselineDecorator = "passOR"
SetupMonoHTruthSelector().SignalHardProcess = True

##
## MUON settings
##
SetupMonoHTruthSelector().doMuons = True
SetupMonoHTruthSelector().TruthBaselineMuonPtCut = 7000
SetupMonoHTruthSelector().TruthBaselineMuonEtaCut = 2.5
SetupMonoHTruthSelector().TruthSignalMuonPtCut = 10000
SetupMonoHTruthSelector().TruthSignalMuonEtaCut = 2.5

##
## ELECTRON settings
##
SetupMonoHTruthSelector().doElectrons = True
SetupMonoHTruthSelector().TruthBaselineElectronPtCut = 7000
SetupMonoHTruthSelector().TruthBaselineElectronEtaCut = 2.47
SetupMonoHTruthSelector().TruthSignalElectronPtCut = 20000
# Crack-Veto
#SetupMonoHTruthSelector().ExcludeBaselineElectronInEta = ["-1.52;-1.37", "1.37;1.52"]

##
## TAU settings
##
SetupMonoHTruthSelector().doTaus = False
SetupMonoHTruthSelector().TruthBaselineTauPtCut = 20000
SetupMonoHTruthSelector().TruthBaselineTauEtaCut = 2.47
# Crack-Veto
SetupMonoHTruthSelector().ExcludeBaselineTauInEta = ["-1.52;-1.37", "1.37;1.52"]

##
## JET (R=0.4) settings
##
SetupMonoHTruthSelector().doJets = True
SetupMonoHTruthSelector().TruthBaselineJetPtCut = 20000
SetupMonoHTruthSelector().TruthBaselineJetEtaCut = 4.5
SetupMonoHTruthSelector().TruthSignalJetPtCut = 20000
SetupMonoHTruthSelector().TruthSignalJetEtaCut = 2.5
SetupMonoHTruthSelector().TruthBJetEtaCut = 2.5

##
## JET (R=1.0) settings
##
SetupMonoHTruthSelector().doFatJets = True
SetupMonoHTruthSelector().TruthBaselineFatJetPtCut = 200000
SetupMonoHTruthSelector().TruthBaselineFatJetEtaCut = 2.0

##
## JET (R=0.2) settings (only avaliable in TRUTH1)
##

SetupMonoHTruthSelector().doTrackJets = not isTRUTH3()
SetupMonoHTruthSelector().TruthBaselineTrackJetPtCut = 10000
SetupMonoHTruthSelector().TruthBaselineTrackJetEtaCut = 2.5

SetupAlgorithm()

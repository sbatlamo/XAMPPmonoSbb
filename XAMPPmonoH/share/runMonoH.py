import argparse
import os
#include the common library instead of importing it. So we have full access to the globals
include("XAMPPmonoH/MonoHToolSetup.py")

AssembleIO()
SetupTransferAssociatedVRJetsAlg()
SetupMonoHAnalysisHelper()
ParseCommonMonoHOptions()

####################
# Generic Settings #
####################
SetupMonoHAnaConfig().ActiveCutflows = [
    "0L_SR_Resolved", "0L_SR_Merged", "1L_CR_Resolved", "1L_CR_Merged", "2L_CR_Ele_Resolved", "2L_CR_Ele_Merged", "2L_CR_Muo_Resolved",
    "2L_CR_Muo_Merged", "2L_CR_EMu_Resolved", "2L_CR_EMu_Merged"
]
# not active cutflows: none

####################
# Trigger Settings #
####################
SetupMonoHAnaConfig().doMETTrigger = True
SetupSUSYTriggerTool().LowestUnprescaledMetTrigger = True
SetupMonoHAnalysisHelper().doMetTriggerSF = True
# add flags for MET / single lepton triggers having fired
SetupMonoHAnaConfig().doSingleElecTrigger = True
SetupMonoHAnaConfig().doSingleMuonTrigger = True
SetupSUSYTriggerTool().WriteSingleElecObjTrigger = True
SetupSUSYTriggerTool().WriteSingleMuonObjTrigger = True
SetupSUSYTriggerTool().MetTrigEmulation = False

# Compare the list of triggers with the information listed on
# https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled
METTriggers = [
    "HLT_xe70_mht",  # 2015: HLT_xe70_mht C2->J
    "HLT_xe90_mht_L1XE50",  # 2016: HLT_xe90_mht_L1XE50 A->D3
    # "HLT_xe100_mht_L1XE50",  # 2016: HLT_xe100_mht_L1XE50 D4->F1 (used by SUSYTools lowest unprescaled triggersbut not by us)
    # "HLT_xe110_mht_L1XE50",  # 2016: HLT_xe110_mht_L1XE50 F2 -   (used by SUSYTools lowest unprescaled triggers but not by us)
    "HLT_xe110_mht_L1XE50",  # 2016: HLT_xe110_mht_L1XE50 D4->F2   (is used by us to reduce number of triggers and simplify MET trigger SF)
    "HLT_xe110_pufit_L1XE55",  # 2017: HLT_xe110_pufit_L1XE55 B->D5
    "HLT_xe110_pufit_L1XE50",  # 2017: HLT_xe110_pufit_L1XE50 D6->K
    # "HLT_xe110_pufit_xe70_L1XE50",  # 2018: HLT_xe110_pufit_xe70_L1XE50  B->C5  (used by SUSYTools lowest unprescaled triggersbut not by us)
    # "HLT_xe110_pufit_xe65_L1XE50"  # 2018: HLT_xe110_pufit_xe65_L1XE50  C5->    (used by SUSYTools lowest unprescaled triggersbut not by us)
    "HLT_xe110_pufit_xe70_L1XE50",  # 2018: HLT_xe110_pufit_xe70_L1XE50  B->-     (is used by us to reduce number of triggers and simplify MET trigger SF)
]

SingleLeptonTriggers = [  #"HLT_e24_lhmedium_L1EM18VH", # MC only equivalent run numbers as for 2015 only, no data -> this trigger is not included in https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled
    "HLT_e24_lhmedium_L1EM20VH",  # data 2015 only, no MC
    "HLT_e60_lhmedium",  # 2015
    "HLT_e120_lhloose",  # 2015
    "HLT_mu20_iloose_L1MU15",  # 2015
    "HLT_mu50",  # 2016 A, B-D3, D4-E, F-G2, G3-I3, I4-, 2017 B-, 2018
    "HLT_e60_lhmedium_nod0",  # 2016 A, B-D3, D4-F, G-, 2017 B-, 2018
    "HLT_e140_lhloose_nod0",  # 2016 A, B-D3, D4-F, G-, 2017 B-, 2018
    "HLT_mu40",  # 2015, 2016 A
    "HLT_mu24_ivarmedium",  # 2016 B-D3, D4-E
    "HLT_mu26_ivarmedium",  # 2016 D4-E, F-G2, G3-I3, I4-, 2017 B-, 2018
    "HLT_e26_lhtight_nod0_ivarloose"  # 2016 D4-F, G-, 2017 B-, 2018
]
# add only lowest unprescaled triggers by calling constrainToPeriods
from XAMPPbase.Utils import constrainToPeriods
# drop 2016 + 2018 trigger from the list of triggers to be constrained to period and
# add it later constrained to the correct period (because at the moment
# mono-h(bb) is not using the lowest unprescaled triggers but instead just
# unprescaled triggers)
METTriggers.remove("HLT_xe110_mht_L1XE50")
METTriggers.remove("HLT_xe110_pufit_xe70_L1XE50")
Triggers = constrainToPeriods(METTriggers) + ["HLT_xe110_mht_L1XE50;302919-311481", "HLT_xe110_pufit_xe70_L1XE50;348885-364485"]
Triggers += constrainToPeriods(SingleLeptonTriggers)
SetupSUSYTriggerTool().Triggers = Triggers

EleTrigSFConfig = [
    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
]
ElectronTriggerSF = [
    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
]
SetupSUSYElectronSelector().SFTrigger = ElectronTriggerSF
SetupSUSYElectronSelector().TriggerSFconfig = EleTrigSFConfig
SetupSUSYElectronSelector().DoTime = True

MuonTriggerSF2015 = ["HLT_mu20_iloose_L1MU15_OR_HLT_mu40"]
MuonTriggerSF2016 = [
    "HLT_mu24_ivarmedium_OR_HLT_mu40",
    "HLT_mu24_ivarmedium_OR_HLT_mu50",
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]
MuonTriggerSF2017 = ["HLT_mu26_ivarmedium_OR_HLT_mu50"]
MuonTriggerSF2018 = ["HLT_mu26_ivarmedium_OR_HLT_mu50"]
SetupSUSYMuonSelector().SFTrigger2015 = MuonTriggerSF2015
SetupSUSYMuonSelector().SFTrigger2016 = MuonTriggerSF2016
SetupSUSYMuonSelector().SFTrigger2017 = MuonTriggerSF2017
SetupSUSYMuonSelector().SFTrigger2018 = MuonTriggerSF2018
SetupSUSYMuonSelector().DoTime = True

######################
# Electron Selection #
######################
SetupSUSYElectronSelector().ApplyTriggerSF = True
# store electron trigger SF in separate branch - the product of all other electron SF should be combined in the electron weight
SetupSUSYElectronSelector().ExcludeTrigSFfromTotalWeight = True
# write second set of scale factors for baseline objects
SetupSUSYElectronSelector().WriteBaselineSF = False

# the isolation requirements for pre-electrons and baseline-electrons are
# already taken into account by SUSYTools because we defined the isolation
# working points for baseline electrons in the susytools config file
SetupSUSYElectronSelector().RequireIsoPreSelection = False
SetupSUSYElectronSelector().RequireIsoBaseline = False

##################
# Muon Selection #
##################
SetupSUSYMuonSelector().ApplyTriggerSF = True
# store muon trigger SF in separate branch - the product of all other muon SF should be combined in the muon weight
SetupSUSYMuonSelector().ExcludeTrigSFfromTotalWeight = True
# write second set of scale factors for baseline objects
SetupSUSYMuonSelector().WriteBaselineSF = False

# the isolation requirements for pre-electrons and baseline-electrons are
# already taken into account by SUSYTools because we defined the isolation
# working points for baseline electrons in the susytools config file
SetupSUSYMuonSelector().RequireIsoPreSelection = False
SetupSUSYMuonSelector().RequireIsoBaseline = False

##################
# Sample merging #
##################
SetupMonoHAnalysisHelper().useVZqqVZbb = True
SetupMonoHAnalysisHelper().useTTBarMETFiltered0LepSamples = True
SetupMonoHAnalysisHelper().useZnunuBfilPTV0LepSamples = True

################################################
# Fix for nasty PTV / MJJ sliced Znunu samples #
################################################
# The buggy Znunu samples are partially fixed in derivations created with
# AthDerivation 21.2.55.0 or more recent.
# The information in the DxAOD has the correct MC channel number
# but unfortunately not the information in the MetaData.
# Since in the python job option we can only access the metadata,
# we need to add an offset to the DSID stored in the metadata.
# For the XAMPP framework we don't need to apply an offset to the
# mcChannelNumber (which is fixed, so we deactivate this offset)
# but we still need to apply the offset to the MetaData DSID.
if not isData() and getMCChannelNumber() >= 366001 and getMCChannelNumber() <= 366008:
    from ClusterSubmission.Utils import ResolvePath
    recoLog = logging.getLogger('XAMPPmonoH custom Znunu fix')
    recoLog.info("Detected buggy Znunu sample... using our own custom fixed PRW files.")
    athArgs = getAthenaArgs()
    if athArgs.dsidBugFix != None:
        recoLog.info("Found the following string in 'dsidBugFix': %s" % (athArgs.dsidBugFix))
        if athArgs.dsidBugFix == "BFilter": offset = 9
        elif athArgs.dsidBugFix == "CFilterBVeto": offset = 18
        elif athArgs.dsidBugFix == "CVetoBVeto": offset = 27
        mc_channel = getMCChannelNumber() + offset
        period_num = getRunNumbersMC()
        mc_campaign = ""
        if period_num == 284500:
            mc_campaign = "mc16a"
        elif period_num == 300000:
            mc_campaign = "mc16d"
        elif period_num == 310000:
            mc_campaign = "mc16e"
        else:
            recoLog.warning("Nothing has been found for the sample with period number " + str(period_num) + ". Killing this job now.")
            raise NameError('Znunu sample fix: period number ' + str(period_num) + 'does not correspond to known MC campaign!')
        config_file = ResolvePath("XAMPPmonoH/PRW/pileup_{mc}_dsid{dsid}_{sim}.root".format(dsid=mc_channel,
                                                                                            mc=mc_campaign,
                                                                                            sim="AFII" if isAF2() else "FS"))
        recoLog.info("Overwriting PRW file, now using: {prw}".format(prw=config_file))
        SetupSUSYTools().PRWConfigFiles = [config_file]
        # the new derivations have the DSID corrected, deactive the shift that XAMPPbase wants to apply
        # to the DSID and apply a shift to the metadata instead
        SetupMonoHAnalysisHelper().MetaDataDDSIDshift = True
        setupEventInfo().SwitchOnDSIDshift = False
        setupEventInfo().DSIDshift = offset

#########################################
# Jet <-> truth matching and correction #
#########################################
# if you want to study these variables, just comment the following lines back in:
#if not isData():
#    module = SetupJetTruthAssociation()
#    module.JetSelector = SetupMonoHJetSelector()
#    SetupMonoHAnalysisHelper().AnalysisModules.append(module)

SetupSUSYTauSelector().DoTime = True
SetupSUSYPhotonSelector().DoTime = True

#######################################################
# Treatment of nasty large generator weights (Sherpa) #
#######################################################
# Only treat large weights from Sherpa generator!
# In other generators (e.g. Powheg), the event weight includes
# the cross-section, so it is quite expected that weights
# from Powheg are larger than e.g. 100.
if not isData():
    recoLog = logging.getLogger('XAMPPmonoH event weight outlier strategy')
    Gens = getFlags().generators()
    print(Gens)
    if "Sherpa" in Gens:
        # possible strategies to deal with large event weights:
        # 0: process without caring about how large the weight is
        # 1: ignoring event
        # 2: instead of taking large weight, setting it to +/- 1.0 (PMG recommendation)
        setupEventInfo().OutlierWeightStrategy = 2
        # set threshold above which (absolute value) an event is treated as an outlier
        # default OutlierWeightThreshold value: 100.
        setupEventInfo().OutlierWeightThreshold = 100.
        recoLog.info("Detected a Sherpa sample!")
        recoLog.info(
            "Applying outlier event strategy {strat} (0: do nothing, 1: remove event, 2: set event weight to +/- 1.) for abs(weight) above {thresh}."
            .format(strat=setupEventInfo().OutlierWeightStrategy, thresh=setupEventInfo().OutlierWeightThreshold))
    elif "Powheg+Pythia8" in Gens:
        recoLog.info("Detected a Powheg+Pythia8 sample! Not applying weight outlier strategy.")
    elif "aMcAtNlo+Pythia8" in Gens:
        recoLog.info("Detected a aMcAtNlo+Pythia8 sample! Not applying weight outlier strategy.")
    elif "MadGraph+Pythia8+EvtGen" in Gens:
        recoLog.info("Detected a MadGraph+Pythia8 sample! Not applying weight outlier strategy.")
    elif "Herwig" in Gens:
        recoLog.info("Detected a Herwig sample! Not applying weight outlier strategy.")
    else:
        recoLog.info("Unknown sample! Not applying weight outlier strategy.")

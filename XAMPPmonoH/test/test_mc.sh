#!/bin/bash

##############################
# Setup                      #
##############################
# prepare environment (assuming this script is located in <basedir>/XAMPPmonoH/test)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh

# definition of folder for storing test results
TESTDIR=test/test_mc
TESTFILE_ORIGIN="root://eoshome.cern.ch//eos/user/x/xmonoh/ci/mc16_13TeV.304123.MadGraphPythia8EvtGen_A14NNPDF23LO_zp2hdm_bb_mzp1400_mA600.deriv.DAOD_EXOT27.e4429_e5984_s3126_r10201_r10210_p3962/DAOD_EXOT27.19145252._000005.pool.root.1"
TESTFILE_LOCAL=test_mc.root
TESTRESULT=testResult_mc.root

##############################
# Process test sample        #
##############################

# create directory for results
mkdir -p ${TESTDIR}
cd ${TESTDIR}
# copy file with xrdcp
if [ ! -f ${TESTFILE_LOCAL} ]; then
    echo "File not found! Copying it from EOS"
    # get kerberos token with service account xmonoh to access central test samples on EOS
    if [ -z ${SERVICE_PASS} ]
    then
      CERN_USER=xmonoh
      echo "Please enter the password for the service account: user ${CERN_USER} (can be obtained in the Gitlab/XAMPPmonoH/Settings/CI/Secret Variables menu)"
      echo "If you belong to the analysis team and already have your CERN USER kerberos token by entering kinit ${USER}@CERN.CH you can skip by entering ctrl + c"
      kinit ${CERN_USER}@CERN.CH
    else
      echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
    fi
    echo xrdcp ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
    xrdcp ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
fi


# clean up old job result
if [ -f ${TESTRESULT} ]; then
    rm ${TESTRESULT}
fi

# run job
python ${BASEDIR}/XAMPPmonoH/python/runAthena.py --evtMax 10000 --testJob --filesInput ${TESTFILE_LOCAL} --outFile ${TESTRESULT} --jobOptions XAMPPmonoH/runMonoH.py | tee log.txt

# Raise error if the run did not finish successfully
tail -n 20 log.txt | grep 'successful run' || exit 1


##############################
# Evalulate cut flows        #
##############################

##############################
# Evalulate cut flows        #
##############################

# 0 lepton region
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 0L_SR_Resolved | tee cutflow_mc0lep_0L_SR_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 0L_SR_Merged | tee cutflow_mc0lep_0L_SR_Merged.txt

# 1 lepton region
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 1L_CR_Resolved | tee cutflow_mc1lep_1L_CR_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 1L_CR_Merged | tee cutflow_mc1lep_1L_CR_Merged.txt

# 2 electron region
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 2L_CR_Ele_Resolved | tee cutflow_mc2lep_2L_CR_Ele_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 2L_CR_Ele_Merged | tee cutflow_mc2lep_2L_CR_Ele_Merged.txt

# 2 muon region
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 2L_CR_Muo_Resolved | tee cutflow_mc2lep_2L_CR_Muo_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 2L_CR_Muo_Merged | tee cutflow_mc2lep_2L_CR_Muo_Merged.txt

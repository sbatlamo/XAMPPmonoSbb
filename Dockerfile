# The release set in this Dockerfile defines the release of the whole project
# and is parsed by every setup script and by the installation instructions.
# Be aware of this effect if you edit the release here!
FROM atlas/athanalysis:21.2.150
ADD . /xampp/XAMPPmonoH
WORKDIR /xampp/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../XAMPPmonoH && \
    make -j4
